
<div id="powerdb-box">
	<form method="post" action="<?php echo CURRENT_AREA;?>" onsubmit='return formCheck();'>
		<div class="powerdb-row">
			<span class="powerdb-label">Name of Object:</span>
			<?php if($data !== null):?>
				<input type="text" id="objectName" name="objectName" value="<?php echo $data['objectName'];?>" />
			<?php else:?>
				<input type="text" id="objectName" name="objectName" />
			<?php endif;?>
		</div>
		
		<h3>Properties</h3>
		
		<div id="properties">
			<?php if($data !== null):?>
			<?php foreach($data['property'] as $key => $val):?>
				<div class="powerdb-row">
					<span class="powerdb-label">Property name:</span>
						<input type="text" class="property-input" name="property[]" value="<?php echo $val;?>" />
						<?php $type = $data['type'][$key];?>
						<select name="type[]" class="selectType">
							<?php foreach(PowerDB::getTypes() as $c):?>
								<?php if($c == $type):?>
									<option value="<?php echo $c;?>" selected><?php echo $c;?></option>	
								<?php else:?>
									<option value="<?php echo $c;?>"><?php echo $c;?></option>	
								<?php endif;?>
							<?php endforeach;?>
						</select><!--<input type="checkbox" name="strip[]" value="<?php echo $key;?>" /><span class="striptag">Strip harmful?</span>-->
				</div>
			
			<?php endforeach;?>
			
			<?php else:?>
			<!-- Property row -->
			<div class="powerdb-row">
				<span class="powerdb-label">Property name:</span>
				<input type="text" class="property-input" name="property[]" />
				<select name="type[]" class="selectType">
					<?php foreach(PowerDB::getTypes() as $t):?>
						<option value="<?php echo $t;?>"><?php echo $t;?></option>				
					<?php endforeach;?>
				</select><!--<input type="checkbox" name="strip[]" value="0" /><span class="striptag">Strip harmful?</span>-->
			</div>


			<!-- Property row -->
			<div class="powerdb-row">
				<span class="powerdb-label">Property name:</span>
				<input type="text" class="property-input" name="property[]" />
				<select name="type[]" class="selectType">
					<?php foreach(PowerDB::getTypes() as $t):?>
						<option value="<?php echo $t;?>"><?php echo $t;?></option>				
					<?php endforeach;?>
				</select><!--<input type="checkbox" name="strip[]" value="1" /><span class="striptag">Strip harmful?</span>-->
			</div>


			<!-- Property row -->
			<div class="powerdb-row">
				<span class="powerdb-label">Property name:</span>
				<input type="text" class="property-input" name="property[]" />
				<select name="type[]" class="selectType">
					<?php foreach(PowerDB::getTypes() as $t):?>
						<option value="<?php echo $t;?>"><?php echo $t;?></option>				
					<?php endforeach;?>
				</select><!--<input type="checkbox" name="strip[]" value="2" /><span class="striptag">Strip harmful?</span>-->
			</div>
			
			<?php endif;?>
			
		</div>
		
		<input type="button" value="Add" id="add-more-properties"/>&nbsp;<input id="reset" type="button" value="Reset" />&nbsp;<input type="submit" name="create" value="Create object!" />
	</form><br />
	<a href="download/powerdb-pack.zip">Download the base package for your web project.</a><br /><br />
	<span style="color: #cccccc;">Developed by David Wiktorsson 2013</span>
</div>



<?php if($data !== null):?>
<div id="powerdb-generated-data-top">
	<span>&raquo; Click to view generated data</span>
</div>
<?php $PD = new PowerDB();?>
<div id="powerdb-generated-data-block"><pre><?php echo htmlentities($PD->generateFromPostData($data));?></pre>
</div>
<?php endif;?>