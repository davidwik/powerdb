<?php 
ThemeHelper::addMoreCSS("css/modules/displaymessage.css");
ThemeHelper::addMoreJS("js/modules/displaymessage.js");
?>
<div id="messageBoxBackground"></div>
<div id="messageBox">
	<div id="messageBoxContent">
		<div id="messageBoxHeading">Test fel</div>
		<div id="messageBoxText"></div>
	</div>
</div>

<?php if(DisplayMessage::getMessage()):?>
<script type="text/javascript">
	$(document).ready(function(){
		showMessageBox("<?php echo DisplayMessage::getTopic();?>", "<?php echo DisplayMessage::getMessage();?>");
	});
</script>

<?php endif;?>