<?php
//ini_set("display_errors", false);

$url = Config::getInstance()->url;

if(isset($_GET['wsdl'])){
	
	header('content-type: text/xml'); 
	$test = new WSDLCreator("TestWSDL",  $url . "/soaptest?wsdl");
	$test->addFile("../lib/external/soap/class.soapuser.php");
	$test->addFile("../lib/external/soap/class.token.php");
	$test->addFile("../lib/external/soap/class.testws.php");
	//$test->includeMethodsDocumentation(true);
	
	
	$test->setClassesGeneralURL($url . "/soaptest?call=TestWS");
	$test->addURLToClass("TestWS", $url . "/soaptest?call=TestWS");
	$test->addURLToClass("SoapUser", $url .  "/soaptest?call=SoapUser");
	$test->addURLToClass("Token", $url . "/soaptest?call=Token");
	
	$test->createWSDL();
	$test->printWSDL();
}
else {
	if(!isset($_SERVER['HTTP_ACCEPT'])){
		$server = new SoapServer($url . "/soaptest?wsdl");
		
		if(isset($_GET['call'])){
			switch($_GET['call']){
				case "Token":
					$server->setClass("Token");
					break;
				case "SoapUser":
					$server->setClass("SoapUser");
					break;
				default;
					$server->setClass("TestWS");
			}
		}
		else {
			$server->setClass("TestWS");
		}
		$server->handle();
	}	
}