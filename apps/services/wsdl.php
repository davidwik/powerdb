<?php echo "<?xml version='1.0' encoding='UTF-8'?>";?>
<definitions name="TestWSDL" 
			 targetNamespace="urn:TestWSDL" 
			 xmlns:typens="urn:TestWSDL" 
			 xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
			 xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" 
			 xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" 
			 xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" 
			 xmlns="http://schemas.xmlsoap.org/wsdl/">
	<message name="Hello"></message>
	<message name="HelloResponse">
		<part name="HelloReturn" type="xsd:string"></part>
	</message>
	
	<message name="__construct">
		<part name="p" type="xsd:boolean"></part>
	</message>
	
	<message name="__constructResponse"></message>
	
	<message name="sub">
		<part name="a" type="xsd:integer"></part>
		<part name="b" type="xsd:integer"></part>
	</message>
	<message name="subResponse">
		<part name="subReturn" type="xsd:integer"></part>
	</message>
	<message name="sum">
		<part name="a1" type="xsd:integer"></part>
		<part name="b1" type="xsd:integer"></part>
	</message>
	<message name="sumResponse">
		<part name="sumReturn" type="xsd:integer"></part>
	</message>
	<portType name="soaptestPortType">
		<operation name="Hello">
			<input message="typens:Hello"></input>
			<output message="typens:HelloResponse"></output>
		</operation>
		<operation name="__construct">
			<input message="typens:__construct"></input>
			<output message="typens:__constructResponse"></output>
		</operation>
		<operation name="sub">
			<input message="typens:sub"></input>
			<output message="typens:subResponse"></output>
		</operation>
		<operation name="sum">
			<input message="typens:sum"></input>
			<output message="typens:sumResponse"></output>
		</operation>
	</portType>
	
	<binding name="soaptestBinding" type="typens:soaptestPortType">
		<soap:binding style="rpc" transport="http://schemas.xmlsoap.org/soap/http"></soap:binding>
		<operation name="Hello">
			<soap:operation soapAction="urn:soaptestAction"></soap:operation>
			<input>
				<soap:body namespace="urn:TestWSDL" 
						   use="encoded" 
						   encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
					
				</soap:body>
			</input>
			<output>
				<soap:body namespace="urn:TestWSDL" 
						   use="encoded" 
						   encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
					
				</soap:body>
			</output>
		</operation>
		<operation name="__construct">
			<soap:operation soapAction="urn:soaptestAction"></soap:operation>
			<input>
				<soap:body namespace="urn:TestWSDL" 
						   use="encoded" 
						   encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">

				</soap:body>
			</input>
			<output>
				<soap:body namespace="urn:TestWSDL" 
						   use="encoded" 
						   encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"></soap:body>
			</output>
		</operation>
		
		<operation name="sub">
			<soap:operation soapAction="urn:soaptestAction"></soap:operation>
			<input>
				<soap:body namespace="urn:TestWSDL" use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
				</soap:body>
			</input>
			<output>
				<soap:body namespace="urn:TestWSDL" 
						   use="encoded" 
						   encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"></soap:body>
			</output>
		
		</operation>
		<operation name="sum">
			<soap:operation soapAction="urn:soaptestAction"></soap:operation>
			<input>
				<soap:body namespace="urn:TestWSDL" 
						   use="encoded" 
						   encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"></soap:body>
			</input>
			<output>
				<soap:body namespace="urn:TestWSDL" 
						   use="encoded" 
						   encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"></soap:body>
			</output>
		</operation>
	</binding>
	<service name="TestWSDLService">
		<port name="soaptestPort" 
			  binding="typens:soaptestBinding">
			<soap:address location="http://unilogin.hexagon.org/soapserv"></soap:address>
		</port>
	</service>
</definitions>