<?php

// Define some time (in seconds constants) 
define('MINUTE_SEC', 60);
define('HOUR_SEC', 3600);
define('DAY_SEC', 86400);
define('WEEK_SEC', 604800);
define('MONTH_SEC', 2592000);
define('YEAR_SEC', 31536000);

?>