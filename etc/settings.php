<?php
$parts = explode(DIRECTORY_SEPARATOR ,__DIR__);

array_pop($parts);
$dir = implode(DIRECTORY_SEPARATOR, $parts);
$url = $_SERVER['SERVER_NAME'];

if(isset($_SERVER['SERVER_PORT'])){
	if($_SERVER['SERVER_PORT'] != "80"){
		$url .= ":" . $_SERVER['SERVER_PORT'];
	}
}


// Site specific settings 
$config['databaseDriver']		= "mysql";
$config['databaseHost']			= "host";
$config['databaseName']			= "name";
$config['databaseUser']			= "user";
$config['databasePassword']		= "password";

$config['root_dir']				= $dir;
$config['url']					= $url;
$config['title']				= 'PowerDB';
$config['webmaster']			= 'contact@email.com';
$config['defaultTemplate']		= 'powerdb';
$config['defaultLanguage']		= 'en';
$config['defaultCurrency']		= 'EUR';
$config['supportedLanguages']	= "en,da,sv,no,pt,de";

$config['startPage']			= 'start';
$config['errorPage']			= 'error404';
$config['deniedPage']			= 'accessdenied';
$config['uniqueString']			= 'enterAr4nd0m5r1ngh3r3';
$config['apc-cache']			= false;
$config['standardPort']			= 80;
$config['SSLPort']				= 443;
$config['secureSite']			= false;  // Enforces game center to work in HTTPS all the time
$config['routes']				= $config['root_dir'] . DIRECTORY_SEPARATOR . "etc" . DIRECTORY_SEPARATOR . "routes.xml";


// Grant access to the Soap Service from these IP addresses
$config['soapAccess']			= array ('127.0.0.1');

// Maintenance Mode - website offline for public except from IP addresses below.
$config['developMode']			= true;		// Displays errors and logs them, if false only logging
$config['maintenance']			= false;
$config['AddrOverride']			= array('127.0.0.1');

