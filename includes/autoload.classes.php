<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

// Constants:
define ('DIRSEP', DIRECTORY_SEPARATOR);

// All filenames should be lowercase!
function __autoload($class_name) {
	$filename = "class." . strtolower($class_name) . '.php';

	$paths = array();
	$paths[] = ROOT_DIR . DIRSEP . 'lib' . DIRSEP . $filename;
	$paths[] = ROOT_DIR . DIRSEP .'lib' . DIRSEP . 'interfaces' . DIRSEP . $filename;
	$paths[] = ROOT_DIR . DIRSEP .'lib' . DIRSEP . 'base' .		DIRSEP . $filename;
	$paths[] = ROOT_DIR . DIRSEP .'lib' . DIRSEP . 'extras' .	 DIRSEP . $filename;
	$paths[] = ROOT_DIR . DIRSEP .'lib' . DIRSEP . 'external'  . DIRSEP . $filename;
	$paths[] = ROOT_DIR . DIRSEP . 'lib' . DIRSEP . 'powerdb' . DIRSEP . $filename;
	$paths[] = ROOT_DIR . DIRSEP . 'lib' . DIRSEP . 'test' . DIRSEP . $filename;


	$inc = false;
	foreach($paths as $p){
		if(file_exists($p)){
			$inc = $p;
		}
	}
	if($inc){
		include($inc);
	}
	else {
		return false;
	}
}
?>