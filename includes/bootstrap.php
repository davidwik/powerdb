<?php
/**
 * SETS UP ENCODING
 */
session_start();
$startTime = microtime(true);
require("../etc/settings.php");
require("../etc/constants.php");
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
mb_http_input('UTF-8');
mb_language('uni');
mb_regex_encoding('UTF-8');


define('ROOT_DIR', $config['root_dir']);
date_default_timezone_set("Europe/Copenhagen");

// For the TCPDF Library 
define('TCPDF_PATH', ROOT_DIR . DIRECTORY_SEPARATOR . "includes" . DIRECTORY_SEPARATOR . "tcpdf");

require(ROOT_DIR . DIRECTORY_SEPARATOR . "includes". DIRECTORY_SEPARATOR . "autoload.classes.php");

// init the config object
Config::setConfig($config);

// Set what URI is requested
Router::setRequest($_SERVER['REQUEST_URI']);

// Set Logging mode
if(Config::getInstance()->developMode){
	ini_set('log_errors','0');
	ini_set('display_errors','1');
}
else {
	ini_set('log_errors','1');
	ini_set('display_errors','0');
}

// If the system is down for maintenance mode redirect ppl
require(ROOT_DIR . DIRECTORY_SEPARATOR . "includes"  . DIRECTORY_SEPARATOR . "offline.php");

/**
 * Constant for the current page
 * @var CURRENT_AREA 
 */
if(!defined('useCLI')){
	if(isset($_SERVER['REQUEST_URI'])){
		define('CURRENT_AREA', substr($_SERVER['REQUEST_URI'], 1));
	}
	else {
		define('CURRENT_AREA', Router::getInstance()->property()->area);
	}
}


require ROOT_DIR . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'init_session.php';

if (version_compare(phpversion(), '5.1.0', '<') == true) { die ('PHP5.1 Only'); }
