<?php
/* 
 * Initiate the session with some added security
 */

$sec_salty = "c!d=Vs{";
if (isset($_SESSION['HTTP_USER_AGENT'])){
	
	if ($_SESSION['HTTP_USER_AGENT'] != md5($_SERVER['HTTP_USER_AGENT'] . $sec_salty)){
		die("Possible hijack attempt, your IP is logged.");
	}
}
else {
	if(isset($_SERVER['HTTP_USER_AGENT'])){
		$_SESSION['HTTP_USER_AGENT'] = md5($_SERVER['HTTP_USER_AGENT'] . $sec_salty);
	}
}?>