<?php
// Go to offline mode? true/false


if(Config::getInstance()->maintenanceMode){
	if(!in_array($_SERVER['REMOTE_ADDR'], Config::getInstance()->addrOverride)){
		header("Location: " . Config::getInstance()->url . "/offline.html");
	}
}
?>