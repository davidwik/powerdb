<?php
/**
 * API for caching, will only use caching if described in settings.php and that APC is enabled.
 * @package Base
 * @author david
 */
class Cache {
	
	/**
	 * Check whether the APC is enabled
	 * @return boolean
	 */
	public static function isEnabled (){
		if(Config::getInstance()->APC){ 
			if(is_callable("apc_add")){
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	/**
	 * Adds a new entry or overrides old data
	 * @param string $name
	 * @param mixed $obj
	 * @param integer $ttl 
	 * @return boolean
	 */
	public static function add($name, $obj, $ttl = 300){
		if(!self::isEnabled()) return false;
		
		return apc_store($name, serialize($obj), $ttl);
		
	}
	
	/**
	 * Gets data stored in cache
	 *  
	 * @param string $name
	 * @return mixed
	 */
	public static function get($name){
		if(!self::isEnabled()) return false;
		
		$data = apc_fetch($name);
		
		if(!$data) return false;
		
		$obj = unserialize($data);
		
		if(!$obj) return false;
		
		return $obj;
	}
	
	/**
	 * Deletes the data from Cache
	 * @param string $name
	 * @return type 
	 */
	public static function remove($name){
		if(!self::isEnabled()) return false;
		
		return apc_delete($name);
		
	}
	
}
?>
