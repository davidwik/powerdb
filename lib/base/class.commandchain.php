<?php
/**
 * Manages so called Commands that will be runned in a chain.
 * If one command fails the chain breaks.
 *
 * @author David Wiktorsson - Serious Games Interactive
 * @package Base
 */

class CommandChain {
	private $_commands;
	private $_debug;

	private $_triggerArray;

	public function  __construct($debug = false) {
		$this->_hasBeenValidated = false;
		$this->_commands = array();
		$this->_triggerArray = array();
		$this->_debug = $debug;

	}

	public function addCommand(ICommand $command){
		$this->_commands[] = $command;
		$this->_triggerArray[] = $command->getTriggerName();
	}
	
	/**
	 * Executes an command with name as identifier
	 * and $args as sent to the command
	 * @param <type> $name
	 * @param array $args
	 * @return <type>
	 */
	public function executeCommand($name, array $args){
		foreach($this->_commands as $cmd){
			$obj = $cmd->execute($name, &$args);
			
			if($obj) {
				return $obj;
			}
		}
	}

	/**
	 * This method executes all commands in the chain, ordered in array $listOfNames
	 * 
	 * @param array $listOfNames
	 * @param array $arguments
	 * return Object
	 */
	public function executeCommandChain(array $args){
		foreach($this->_triggerArray as $name){
			if($this->_debug){
				echo "Running Command: <b>" . $name . "</b><br />";
			}

			$ob = $this->executeCommand($name, &$args);
			if(is_object($ob) && !$ob->success){
				return $ob;
			}
		}
		return (object) array ('success' => true);
	}
}
?>