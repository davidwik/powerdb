<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of class
 *
 * @author david
 * @package Base
 */
class Config implements ISingleton {
	/**
	 * Instance of the configuration object
	 * @var Config 
	 */
	private static $_instance;
	private static $_config;

	/**
	 * The root dir of the project
	 * @var string
	 */
	public $root_dir;
	
	/**
	 * URL for the project
	 * @var string
	 */
	public $url;
	
	/**
	 * The title for the web project
	 * @var string
	 */
	public $title; 
	
	/**
	 * Email to the webmaster
	 * @var string
	 */
	public $webmaster;
	
	/**
	 * Which template should be used as default (theme)
	 * @var string
	 */
	public $defaultTemplate;
	
	/**
	 * Path to the routes.xml
	 * @var string 
	 */
	public $routes;
	
	/**
	 * Which area is the start area
	 * @var string
	 */
	public $startPage;
	
	/**
	 * Which is the default error page
	 * @var string
	 */
	public $errorPage;
	
	/**
	 * Which is the default access denied page
	 * @var string
	 */
	public $deniedPage;
	
	/**
	 * A unique id being used in different security routines
	 * @var string
	 */
	public $unique;
	
	/**
	 * If true, the entire site is forced to use https
	 * @var boolean
	 */
	public $secureSite;

	/**
	 * The default language of the website 
	 * @var string
	 */
	public $defaultLanguage;
	
	/**
	 * Array of supported languages
	 * @var array
	 */
	public $supportedLanguages;
	
	/**
	 * Default currency 
	 * @var array
	 */
	public $defaultCurrency;

	/**
	 * Which port is http running on (80)
	 * @var integer
	 */
	public $standardPort;
	
	/**
	 * Which port is https running on (443) 
	 * @var integer
	 */
	public $SSLPort;
	
	/**
	 * Should APC be enabled ( caching )
	 * @var boolean
	 */
	public $APC;
	
	/**
	 * On true the user will be redirected to a "under maintenace page"
	 * @var boolean
	 */
	public $maintenanceMode;
	
	/**
	 * Shows errors on page, and collects extra debug information
	 * @var boolean
	 */
	public $developMode;
	
	/**
	 * Array of ip number that should be allowed when site is under maintenance mode. 
	 * @var array
	 */
	public $addrOverride;
	
	/**
	 * Array of ip numbers that should be able to access the soap web service.
	 * @var array
	 */
	public $soapAccess;
	
	/**
	 * The driver for the database 'mysql', 'pgsql'
	 * @var string
	 */
	public $databaseDriver;
	
	/**
	 * The host for the database
	 * @var string
	 */
	public $databaseHost;
	
	/**
	 * The name of the database
	 * @var string
	 */
	public $databaseName;
	
	/**
	 * The user for the database
	 * @var string
	 */
	public $databaseUser;
	
	/**
	 * The password for the database
	 * @var string
	 */
	public $databasePassword;
	

	private function __construct() {
		if(Config::$_config === null){
			throw new Exception("Need to set config first, use Config::setConfig(configvar)");
		}
		$this->addrOverride = array();
		$this->root_dir = Config::$_config['root_dir'];
		$this->url = Config::$_config['url'];
		$this->title = Config::$_config['title'];
		$this->webmaster = Config::$_config['webmaster'];
		$this->defaultTemplate = Config::$_config['defaultTemplate'];
		
		$this->defaultCurrency = Config::$_config['defaultCurrency'];
		
		$this->routes = Config::$_config['routes'];
		$this->startPage = Config::$_config['startPage'];
		$this->developMode = Config::$_config['developMode'];

		$this->standardPort = Config::$_config['standardPort'];
		$this->SSLPort = Config::$_config['SSLPort'];

		
		$this->maintenanceMode = Config::$_config['maintenance'];
		$this->addrOverride = Config::$_config['AddrOverride'];
		
		$this->soapAccess = Config::$_config['soapAccess'];

		$this->errorPage = Config::$_config['errorPage'];
		$this->deniedPage = Config::$_config['deniedPage'];
		$this->unique = Config::$_config['uniqueString'];
		$this->defaultLanguage = Config::$_config['defaultLanguage'];
		$this->supportedLanguages = Config::$_config['supportedLanguages'];
		$this->APC = Config::$_config['apc-cache'];
		$this->secureSite = Config::$_config['secureSite'];
		
		$this->databaseDriver = Config::$_config['databaseDriver']; 
		$this->databaseHost = Config::$_config['databaseHost'];
		$this->databaseName = Config::$_config['databaseName'];
		$this->databaseUser = Config::$_config['databaseUser'];
		$this->databasePassword = Config::$_config['databasePassword'];
		
		
	}

	public static function setConfig($c){
		self::$_config = $c;
	}

	/**
	 * Gets an instance of the config object
	 * @return Config
	 */
	public static function getInstance(){
		if(self::$_config === null){
			throw new Exception("Need to set config first, use Config::setConfig(configvar)");
		}
		if(self::$_instance === null){
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	public static function destroy() {
		self::$_config = null;
	}
	
	/**
	 * Returns an array of the supported languages in the system.
	 * @return array
	 */
	public function getSupportedLanguages(){
		return explode(",", $this->supportedLanguages);
	}
	
}
?>