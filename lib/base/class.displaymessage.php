<?php
/**
 * This object is only for static usage.
 * It handles the informal messages to the user. 
 * @author David Wiktorsson
 * @package Base
 */
class DisplayMessage {
	private static $_ErrorMsg = false;
	private static $_Topic = false;

	private function __construct(){}

	public static function setMessage($str, $topic = false){
		self::$_ErrorMsg = $str;
		self::$_Topic = $topic;
	}
	

	public static function getMessage(){
		return (self::$_ErrorMsg) ? self::$_ErrorMsg : false;
	}

	public static function getTopic(){
		return (self::$_Topic) ? self::$_Topic : " ";
	}
	
	/*public static function __toString(){
		return (self::$_ErrorMsg) ? self::$_ErrorMsg : false;
	}*/
}
?>