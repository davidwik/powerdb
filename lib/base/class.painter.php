<?php
/**
 * Painter a decoration class.
 *
 * @author David Wiktorsson
 * @package Base
 */
class Painter {
	private static $_params;
	
	private function  __construct(){}
	
	public static function getPainter(IPainter $name){
		return $name;
	}

	public static function setProperty($name, $value){
		if(self::$_params === null){
			self::$_params = new stdClass();
		}
		self::$_params->$name = $value;
	}

	public static function getProperty($name = false){
		if(!$name){
			return self::$_params;
		}
		else {
			return (isset(self::$_params->$name)) ? self::$_params->$name : null;
		}
	}
}
?>