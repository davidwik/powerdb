<?php

/**
 * Page properties for the Router Class. 
 * @package Base
 * @author David Wiktorsson
 */
class PageProperties {
	public $area;
	public $location;
	public $access;
	public $title;
	public $js;
	public $style;
	public $type;
	public $theme;
	public $ssl;

	/**
	 *
	 * @param array $page Consist of all the properties for a page
	 */
	public function __construct($page){
		$this->area = (isset($page['area'])) ? $page['area'] : false;
		$this->location = (isset($page['location'])) ? $page['location'] : false;
		$this->access = (isset($page['access'])) ? $page['access'] : false;
		$this->title = (isset($page['title'])) ? $page['title'] : false;
		$this->js = (isset($page['js'])) ? $page['js'] : false;
		$this->style = (isset($page['style'])) ? $page['style'] : false;
		$this->type = (isset($page['type'])) ? $page['type'] : false;
		$this->theme = (isset($page['theme'])) ? $page['theme'] : false;
		$this->ssl = (isset($page['ssl'])) ? $page['ssl'] : false;
	}
}


/**
 * The Router object keeps track which page to load, permissions and
 * also holds the properties for the current page.
 *
 * @author David Wiktorsson - Serious Games Interactive
 * @package Base
 */
class Router implements ISingleton {
	private $_cachedXML; 
	private $_hit;
	private static $_instance;
	private $_properties;
	private $_pages;
	
	// The request!
	private static $_request;
	
	/**
	 * Holds the arguments for the request. For instance api/users/3 goes to the user with id 3.
	 * Will be represented as [0] page, [1] call, [2] etc.. 
	 * @var array
	 */
	public $args;

	/**
	 * Sets the request
	 * @param string $request
	 */
	public static function setRequest($request){
		if($request == '/'){
			self::$_request = Config::getInstance()->startPage;
		}
		else {
			self::$_request = substr($request, 1);
		}	
	}
	
	/**
	 * Return the request 
	 * @return string
	 */
	public static function getRequest(){
		return self::$_request;
	}
	
	public static function getCleanRequest(){
		$requestLength = strlen(self::getRequest());
		
		$leng1 = strpos(self::getRequest(), '&');
		$leng2 = strpos(self::getRequest(), '?');
		$cut = $requestLength;
		if($leng1 != false){
			$cut = $leng1;
		}
		else if($leng2 != false ){
			$cut = $leng2;
		}
				
		$res = substr(self::$_request, 0, $cut);
		
		return $res;
		
	}
	
	private function __construct(){
		$this->args = array();
		if(isset($_GET['logout'])){
			$c = Config::getInstance();
			$_SESSION = array();
			header("Location: " . $c->url . "/" . $c->startPage);
		}

		$this->_pages = $this->loadData();  // Load the XML or the CACHE
		
		$this->args = explode("/", self::getCleanRequest());
		
		if(isset($this->args[0])){   // Load the current page before throwing in the layout
			foreach($this->_pages as $PageProperty){
				if(!strcmp($this->args[0], $PageProperty->area)){
					$this->_properties = $PageProperty;
				}
			}
		}
	}

	
	public function getAllPages(){
		return $this->_pages;
	}

	/**
	 * Reads the data from xml, caches it
	 * @return Array 
	 */
	private function loadData(){
		
		if(Config::getInstance()->APC == false || !$this->_cachedXML = Cache::get(Config::getInstance()->unique . '_allowedURLS')){

			$pageArray = array();
			$dom = new DOMDocument();
			if(!$dom->load(Config::getInstance()->routes)){
				die("Error loading URL");
			}
			$xml = $dom->getElementsByTagName("url");
			$elements = 0;

			foreach($xml as $node){
				$page['area'] = $node->getElementsByTagName("id")->item(0)->nodeValue;
				$page['location'] = ($node->getElementsByTagName("location")->item(0)) ? $node->getElementsByTagName("location")->item(0)->nodeValue : false;
				$page['title'] = ($node->getElementsByTagName("title")->item(0)) ? $node->getElementsByTagName("title")->item(0)->nodeValue : false;
				$page['js'] = ($node->getElementsByTagName("js")->item(0)) ? $node->getElementsByTagName("js")->item(0)->nodeValue: false;
				$page['style'] = ($node->getElementsByTagName("style")->item(0)) ? $node->getElementsByTagName("style")->item(0)->nodeValue : false;
				$page['access'] = $node->getElementsByTagName("access")->item(0)->nodeValue;
				$page['type'] = $node->getElementsByTagName("type")->item(0)->nodeValue;
				$page['theme'] = ($node->getElementsByTagName("theme")->item(0)) ? $node->getElementsByTagName("theme")->item(0)->nodeValue : false;
				$page['ssl'] = ($node->getElementsByTagName("ssl")->item(0)) ? $node->getElementsByTagName("ssl")->item(0)->nodeValue : false;
				$pageArray[] = new PageProperties($page);
			}
			if(Config::getInstance()->APC){
				Cache::add(Config::getInstance()->unique .'_allowedURLS', $pageArray);
			}
		}
		if(!isset($pageArray)){
			$pageArray = $this->_cachedXML;
		}
		return $pageArray;
	}

	/**
	 * Manages the routing on the page
	 * @param int $userLevel
	 */
	public function route($userLevel = 0){
	
		if(!$this->property()->area){
			if(isset($this->args[0])){
				self::reRoute(Config::getInstance()->errorPage . "?req=" . self::getCleanRequest());
			}
			else {
				self::reRoute(Config::getInstance()->errorPage);
			}
		}


		if($this->property()->ssl){
			// reroute to ssl
			if(!isset($_SERVER['HTTPS'])){
				$url = Config::getInstance()->url;
				if(Config::getInstance()->SSLPort != 443){
					//$url = substr($url, 0, -1);
					$url .= ":" . Config::getInstance()->SSLPort;
				}
				$url = str_replace("http", "https", $url);
				header("Location: " . $url . '/' .$this->property()->area);
			}
		}

		if($this->property()->access){
			if($this->property()->access == "*"){
				$this->_hit = 0;
				$this->_hit++;
			}
			else {
				$accessList = explode(";", $this->property()->access);
				$this->_hit = 0;
				$point = $userLevel;
				foreach($accessList as $listId){
					if($listId == $point)
						$this->_hit++;
				}
			}
		}
		else {
			$this->_hit = 1;
		}
		if(!$this->_hit){
			self::reRoute(Config::getInstance()->deniedPage);
			die();
		}
		else {
			if(!strcmp($this->property()->type, "service")){
				require ROOT_DIR . DIRECTORY_SEPARATOR .  "apps" . DIRECTORY_SEPARATOR . "services". DIRECTORY_SEPARATOR . $this->property()->location;
			}
			else {
				// check if any action file exists
				$myArr = explode(DIRECTORY_SEPARATOR, $this->property()->location);

				$actionPath = "";
				for($i = 0; $i < count($myArr) - 1; $i++){
					$actionPath .= $myArr[$i] . DIRSEP;
				}

				$actionPath .= "action." . $myArr[count($myArr)-1];

				if(file_exists(ROOT_DIR . DIRECTORY_SEPARATOR . "apps" . DIRECTORY_SEPARATOR . "actions" . DIRECTORY_SEPARATOR . $actionPath)){
					require ROOT_DIR . DIRECTORY_SEPARATOR . "apps" . DIRECTORY_SEPARATOR . "actions" . DIRECTORY_SEPARATOR . $actionPath;
				}
				// load frontend

				if(file_exists(ROOT_DIR . DIRECTORY_SEPARATOR . "apps" . DIRECTORY_SEPARATOR ."frontend" . DIRECTORY_SEPARATOR . $this->property()->location) && strlen($this->property()->location) > 0 ){
					require ROOT_DIR . DIRECTORY_SEPARATOR . "apps" . DIRECTORY_SEPARATOR . "frontend" . DIRECTORY_SEPARATOR . $this->property()->location;
				}
				else {
					require ROOT_DIR . DIRECTORY_SEPARATOR . "apps" . DIRECTORY_SEPARATOR . "frontend" . DIRECTORY_SEPARATOR . "default.php";
				}
			}
		}

	}

	/**
	 * Re-routes the user to anotherpage.
	 * @param string $page
	 */
	public static function reRoute($page){
		header("Location: " . Config::getInstance()->url . "/" . $page);
		die("");
	}

	/**
	 * Gets the PageProperties
	 * @return PageProperties
	 */
	public function property(){
		if(isset($this->_properties)){
			return $this->_properties;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Returns a router object
	 * @return Router
	 */
	public static function getInstance() {
		if(self::$_instance === null){
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	public static function destroy() {
		self::$_instance = null;
	}
}