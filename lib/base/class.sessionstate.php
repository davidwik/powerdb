<?php
/**
 * SessionState - collecting data within a session serialized object. 
 *
 * @author David Wiktorsson
 * @package Base
 */
class SessionState implements ISingleton {

	private $_varList;
	private static $_instance;

	/**
	 * Creates a SessionVar array if exists from session, if not it creates a new array.
	 */
	private function __construct(){
		$this->_varList = array();
	}

	public function  __destruct() {
		$_SESSION['SessionState'] = serialize($this);
	}

	/**
	 * Saves the session and dies.
	 */

	public static function destroy(){
		$this_varList = array();
		unset($_SESSION['SessionState']);
	}
	
	

	/**
	 * Removes a variable, returns true if var object has been removed
	 * @param string $name
	 * @return boolean
	 */
	public function remove($name){
		$v = false;
		foreach($this->_varList as $key => $obj){
			if(!strcmp($obj->getName(), $name)){
				unset($this->_varList[$key]);
				$v = true;
			}
		}
		sort($this->_varList);
		return $v;
	}

	/**
	 * Returns the SessionVar array
	 * @return array
	 */
	public function dump(){
		return $this->_varList;
	}

	/**
	 *
	 * @param string $name The name of the variable
	 * @param mixed $value The value
	 * @param string $type The type
	 */
	public function setVar($name, $value, $type = "string"){
		$obj = $this->getObj($name);
		if(!strcmp($obj->getName(), "undefined")){
			$var = new SessionVar($name, $value, $type);
			$this->_varList[] = $var;
		}
		else {
			$obj->setType($type);
			$obj->setValue($value);
		}
		// Save it
	}

	/**
	 * Checks whether the variable is set.
	 * @param string $name
	 * @return boolean
	 */
	public function is_set($name){
		$obj = $this->getObj($name);
		return (!strcmp($obj->getName(), "undefined")) ? false : true;
	}

	/**
	 * Returns number of items in SessionState
	 * @return int
	 */
	public function numberOfItems(){
		return count($this->_varList);
	}


	/**
	 * Gets an object, if no object exists with the related name, it creates
	 * an object with undefined as name
	 * @param string $name
	 * @return SessionVar
	 */
	public function getObj($name){
		$obj = false;
		if(count($this->_varList) > 0){
			foreach($this->_varList as $sessionvar){
				if(!strcmp($sessionvar->getName(), $name)){
					$obj = $sessionvar;
				}
			}
			if($obj instanceof SessionVar){
				return $obj;
			}
			else {
				return new SessionVar("undefined", false, "boolean");
			}
		}
		else {
			return new SessionVar("undefined", false, "boolean");
		}
	}

	/**
	 *
	 * @return SessionState
	 */
	public static function getInstance(){
		if(self::$_instance === null){
			if(isset($_SESSION['SessionState'])){
				self::$_instance = unserialize($_SESSION['SessionState']);
			}
			else {
				self::$_instance = new self();
			}
		}
		return self::$_instance;
	}
}?>