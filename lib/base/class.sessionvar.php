<?php
/**
 * A part of the SessionState. 
 *
 * @author David Wiktorsson
 * @package Beta
 */
class SessionVar {
	private $_name;
	private $_value;
	private $_type;


	public function __construct($name, $value, $type = "string"){
		$this->_name = $name;
		$this->_value = $value;
		$this->_type = strtolower($type);
	}

	public function getName(){
		return $this->_name;
	}

	public function setValue($value){
		$this->_value = $value;
	}

	public function setType($type){
		$this->_type = strtolower($type);
	}

	public function getType(){
		return $this->_type;
	}

	public function compare($val){
		if(!strcmp($this->getType(), "string")){
			return (!strcmp($this->getValue(), $val)) ? true : false;
		}
		else {
			return ($this->getValue() == $val) ? true : false;
		}
	}

	public function getValue(){
		switch($this->_type){
			case "string":
				return (string) $this->_value;
				break;
			case "int":
				return (int) $this->_value;
				break;
			case "float":
				return (float) $this->_value;
				break;
			case "double":
				return (double) $this->_value;
				break;
			case "boolean":
				return (boolean) $this->_value;
				break;
			case "bool":
				return (boolean) $this->_value;
				break;
			case "array":
				return (array) $this->_value;
				break;
			case "object":
				return (array) $this->_value;
				break;
			default:
				return (string) $this->_value;	
		}
	}

	public function  __toString() {
		return $this->_name;
	}
}
?>
