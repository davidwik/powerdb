<?php
/**
 * Handles modules
 * @package Base
 * @author David Wiktorsson
 */
class SiteModule {
	
	/**
	 * Gets a module if it exists
	 * @param string $name Name of the module
	 * @param string $style Custom css string 
	 */
	public static function get($name, $style = false, $args = array()){
		$filename = Config::getInstance()->root_dir . DIRSEP . "apps" . DIRSEP . "modules" . DIRSEP . "module." . $name . ".php";
		
		if(file_exists($filename)){
			include($filename);
		}
		else  {
			throw new Exception("File not found for module: " . $name );
		}
	}
}
?>
