<?php
/**
 * A wrapper for webresponses. Useful when returning data to a client. 
 *
 * @author David Wiktorsson
 * @package Base
 */
class StandardResponse {
	public $status;
	public $message;
	public $data;

	/**
	 * Stucture of a communication message, usually wrapped with JSON
	 * @param boolean $status
	 * @param string $message
	 * @param array $data
	 */
	public function __construct($status, $message = "", $data = array()){
		$this->status = $status;
		$this->message = $message;
		$this->data = $data;
	}
}