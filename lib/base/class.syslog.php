<?php
/**
 * A logger class, which uses log/sys.log
 * Make sure this file is writeable. 
 * @package Base
 * @author David
 */
class Syslog {
	
	
	const LOG_LEVEL_DEBUG = 0;
	const LOG_LEVEL_WARN = 1;
	const LOG_LEVEL_CRIT = 2;
	
	/**
	 * Logs an entry in the sys.log 
	 * @param string $msg
	 * @param string $file use __FILE__ for current file
	 * @param string $line  use __LINE__ for affected line
	 * @param string $function use __FUNCTION__ to get the name of the function.
	 */
	public static function log($msg, $errorLevel = 0, $file = null, $line = null, $function = null){
		$logLevel = "";
		
		switch ($errorLevel){
			case self::LOG_LEVEL_DEBUG:
				if(!Config::getInstance()->developMode){
					return true;
				}
				else {
					$logLevel = "DEBUG";
				}
				break;
				
			case self::LOG_LEVEL_WARN: 
				$logLevel = "WARN";
				break;
			
			case self::LOG_LEVEL_CRIT: 
				$logLevel = "CRITICAL";
				break;
			default:
				$logLevel = "DEBUG";
				break;
			
		}
		
		
		$logFile = CONFIG::getInstance()->root_dir . DIRECTORY_SEPARATOR . "logs" . DIRECTORY_SEPARATOR . "sys.log";
		if(is_writable($logFile)){
			$handle = @fopen($logFile, "a");
			if($handle != NULL){
				$logMsg = "\n\033[1;34m>> Log entry [" . $logLevel . "]: " . Utils::getDateFromTimestamp(time(), true) . ":" . date('s') .  "\033[0m \n";
										
				if($file){
					$logMsg .= "In\033[1m " . $file . "\033[0m\n";
					if($function){
						$logMsg .= "In function\033[1;32m " . $function . "\033[0m\n";
					}
					if($line){
						$logMsg .= "\033[1;31mNear [line " . $line . "]\033[0m \n";
					}
				}
				
				$logMsg .= "\033[0;37m" . $msg  . "\033[0m\n";
				@fwrite($handle, $logMsg);
				@fclose($handle);
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
}
?>
