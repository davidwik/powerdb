<?php
/**
 * A Helper class to enable that the right javascript and stylesheets are loaded
 * 
 * @see Router
 * @see Site
 * @author David Wiktorsson - Serious Games Interactive
 * @package Base
 */
class ThemeHelper {

	private $_router;

	private static $_additionalCSS;
	private static $_additionalJS;
	
	/**
	 *
	 * @param stdClass $site
	 */
	public function  __construct() {
		$this->_router = Router::getInstance();
	}

	public function printSiteCSS(){
		if(Router::getInstance()->property()->theme){		
			$str =  "\n\t<link href=\"themes/" . Router::getInstance()->property()->theme . "/css/base.css\" rel=\"stylesheet\" type=\"text/css\" />";
		}
		else {
			$str =  "\n\t<link href=\"themes/" . Config::getInstance()->defaultTemplate . "/css/base.css\" rel=\"stylesheet\" type=\"text/css\" />";
		}
		return $str;
	}

	public function printSiteJavaScripts(){
		$str = "\n\t<script language=\"javascript\" type=\"text/javascript\" src=\"js/jquery.js\"></script>";
		
		if(Router::getInstance()->property()->theme){
			$str .= "\n\t<script language=\"javascript\" type=\"text/javascript\" src=\"themes/" . Router::getInstance()->property()->theme . "/js/base.js\"></script>";
		}
		else {
			$str .= "\n\t<script language=\"javascript\" type=\"text/javascript\" src=\"themes/" . Config::getInstance()->defaultTemplate . "/js/base.js\"></script>";
		}
		
		
		return $str;
	}

	public function printTitle(){
		return "\n\t<title>" . Config::getInstance()->title . " - " . $this->_router->property()->title . "</title>\n";
	}


	/**
	 *
	 * @return String
	 */
	public function getHeadtags(){
		$ret = $this->printTitle();
		$ret .= $this->printBaseTag();
		$ret .= $this->printMetaTags();
		$ret .= $this->printSiteCSS();
		$ret .= $this->printPageCSS();
		$ret .= $this->printSiteJavaScripts();
		$ret .= $this->printPageJavaScripts();
		return $ret;
	}
	
	private function printBaseTag(){
		$ret = "\n\t<base href=\"http://" . Config::getInstance()->url . "/\" />";
		return $ret;
	}
	
	private function printMetaTags(){
		$ret = "\n\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
		return $ret;
	}

	public function printPageCSS(){
		$ret = (strlen($this->_router->property()->style) > 1) ? "\n\t<link href=\"" . $this->_router->property()->style . "\" rel=\"stylesheet\" type=\"text/css\" />" : "";
		$moreCSS = self::getAdditionalCSS();
		if($moreCSS){
			foreach($moreCSS as $file){
				$ret .= "\n\t<link href=\"" . $file . "\" rel=\"stylesheet\" type=\"text/css\" />";
			}
		}
		$ret .= "\n";
		return $ret;
	}

	public function printPageJavaScripts(){
		$ret = (strlen($this->_router->property()->js) > 1) ?  "\n\t<script language=\"javascript\" type=\"text/javascript\" src=\"" . $this->_router->property()->js . "\"></script>" : "";
		$moreJS = self::getAdditionalJS();
		if($moreJS){
			foreach($moreJS as $file){
				$ret .=  "\n\t<script language=\"javascript\" type=\"text/javascript\" src=\"" . $file . "\"></script>";
			}
		}
		return $ret . "\n";
	}


	public static function addMoreCSS($pathToCSS){
		if(self::$_additionalCSS === null){
			self::$_additionalCSS[] = $pathToCSS;
		}
		else {
			if(!in_array($pathToCSS, self::$_additionalCSS)){
				self::$_additionalCSS[] = $pathToCSS;
			}
		}
	}

	public static function addMoreJS($pathToJS){
		if(self::$_additionalJS === null){
			self::$_additionalJS[] = $pathToJS;
		}
		else {
			if(!in_array($pathToJS, self::$_additionalJS)){
				self::$_additionalJS[] = $pathToJS;
			}
		}
	}

	public static function getAdditionalCSS(){
		return isset(self::$_additionalCSS) ? self::$_additionalCSS : false;
	}

	public static function getAdditionalJS(){
		return isset(self::$_additionalJS) ? self::$_additionalJS : false;
	}
}
