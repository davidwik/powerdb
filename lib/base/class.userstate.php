<?php
/**
 * @package Base
 * @author David Wiktorsson
 * UserState class helps managing the user in a session.
 */
class UserState implements ISingleton {
	
	private static $_instance;
	
	private static $_sessionName = "UserState";
	
	/**
	 * Identifier of a user.
	 * @var integer
	 */
	public $id;
	
	/**
	 * The user's name
	 * @var string
	 */
	public $name;
	
	/**
	 * Is the user logged in
	 * @var boolean
	 */
	public $loggedIn; 
	
	/**
	 * The level of the user
	 * @var integer
	 */
	public $level;
	
	/**
	 * Last action
	 * @var integer
	 */
	public $lastAction;
	
	
	private function __construct() {
		$this->level = 0;
		$this->name = "Unknown User";
		$this->loggedIn = false;
		$this->id = 0;
	}
	
	public static function destroy() {
		unset($_SESSION[self::$_sessionName]);
		unset(self::$_instance);
	}
	
	public function __destruct(){
		$_SESSION[self::$_sessionName] = serialize(self::$_instance);
	}
	
	/**
	 * Returns a UserState
	 * @return UserState
	 */
	public static function getInstance() {
		if(self::$_instance === null){
			if(isset($_SESSION[self::$_sessionName])){
				self::$_instance = unserialize($_SESSION[self::$_sessionName]);
			}
			else {
				self::$_instance = new self();
			}
		}
		self::$_instance->lastAction = time();
		return self::$_instance;
	}
}
?>