<?php
/**
 * Utils - Misc functions
 * A collection of useful functions.
 * @author David Wiktorsson - Serious Games Interactive
 * @package Base
 */

class Utils {

	/**
	 * Returns the date of today
	 * @return String
	 */
	public static function getDateString() {
		$date = date('Y') . "-" . date('m') . "-" . date('d') . " " . date('H') . ":" . date('i') . ":" . date('s');
		return $date;
	}

	/**
	 * Gets the name of the day from a timestamp
	 * @param int $timestamp
	 * @return string
	 */
	public static function getDayName($timestamp){
		$ret = "";
		switch(date('l', $timestamp)){
			case 'Sunday':
				$ret = "Søndag";
				break;
			case 'Monday':
				$ret = "Mandag";
				break;
			case 'Tuesday':
				$ret = "Tirsdag";
				break;
			case 'Wednesday':
				$ret = "Onsdag";
				break;
			case 'Thursday':
				$ret = "Torsdag";
				break;
			case 'Friday':
				$ret = "Fredag";
				break;
			case 'Saturday':
				$ret = "Lørdag";
				break;
		}
		return $ret;
	}

	/**
	 * Gets the name of the months
	 * @param int $timestamp
	 * @return string
	 */
	public static function getMonthName($timestamp){
		$ret = "";
		switch(date('F', $timestamp)){
			case 'January':
				$ret = "Januar";
				break;
			case 'February':
				$ret = "Februar";
				break;
			case 'March':
				$ret = "Marts";
				break;
			case 'May':
				$ret = "Maj";
				break;
			case 'June':
				$ret = "Juni";
				break;
			case 'July':
				$ret = "Juli";
				break;
			case 'October':
				$ret = "Oktober";
				break;
			default:
				$ret = date('F', $timestamp);
				break;
		}
		return $ret;
	}
	
	/**
	 * Returns the year of the given timestamp
	 * @param int $timestamp
	 * @return int
	 */
	public static function getYear($timestamp){
		return date('Y', $timestamp);
	}

	/**
	 * Returns the month numerical
	 * @param int $timestamp
	 * @return int
	 */
	public static function getMonth($timestamp, $leading_zeroes = false){
		return ($leading_zeroes) ? date('m', $timestamp) : date('n', $timestamp);
	}
	
	/**
	 * Returns the day of the month numerical
	 * @param int $timestamp
	 * @return int
	 */
	public static function getDay($timestamp, $leading_zeroes = false){
		return ($leading_zeroes) ? date('d', $timestamp) : date('j', $timestamp);
	}

	/**
     * Generates a random string
     * @param int $length
     * @return String
     */
	public static function getRandomString($length){
		$Str = '';
		srand((double)microtime()*1000000);
		$chars = array (
                                  'a',
                                  'b',
                                  'c',
                                  'd',
                                  'e',
                                  'Q',
                                  'W',
                                  'I',
                                  'x',
                                  'X',
                                  'y',
                                  'f',
                                  'g',
                                  '1',
                                  '2',
                                  '3',
                                  '4',
                                  '5',
                                  '6',
                                  '7',
                                  '8',
                                  '9',
                                  '0',
                                  );
		for($rand = 0; $rand < $length; $rand++){
			$random = rand(0, count($chars)-1);
			$Str .= $chars[$random];
		}
		return $Str;

	}


	/**
	 * Prints out the date from a unix timestamp
	 * @param int $timestamp
	 * @return string
	 */
	public static function getDateFromTimestamp($timestamp, $includeTime = false){
		$str = date('Y', $timestamp);
		$str .= "-" . date('m', $timestamp);
		$str .= "-" . date('d', $timestamp);
		if($includeTime){
			$str .= " ";
			$str .= date('H', $timestamp);
			$str .= ":" . date('i', $timestamp);
		}
		return $str;

	}

	/**
	* Returns client's ip to string if no args
	* If true is passed it returns the client'ip as an int.
	*
	* @param bool
	* @return string|int
	*/
	public static function getClientIP($asInt = false){
		if(!$asInt){
			return $_SERVER['REMOTE_ADDR'];
		}
		else {
			return ip2long($_SERVER['REMOTE_ADDR']);
		}
	}

	/**
	* Checks if the e-mail address structure is valid
	* @param String $email
	* @return boolean
	*/
	public static function validateEmail($email){
		$email = strtolower($email);
		return preg_match("/^[a-z0-9\.\\-_]+@[a-z0-9.-]+\.[a-z]{2,6}$/i", $email);
	}

	/**
     * Gets filenames from a directory as an array
     * @param String $dir
     * @return Array
     *
     */
	public static function getFilesFromDir($dir){
		$retArr = array();
		$handle = opendir($dir);
		if($handle){
			$cnt = 0;
			while(false !== ($file = readdir($handle))){
				if(strlen($file) > 2){
					$retArr[$cnt]['name'] = $file;
					$retArr[$cnt]['size'] = filesize($dir . $file);
					$cnt++;
				}
			}
			return $retArr;
		}
		else {
			return false;
		}
	}

	/**
	 * check whether timestamp is today
	 * Also supports hours back 
	 * @param int $timestamp
	 * @param int $hours_backward
	 * @return boolean
	 */
	public static function isToday($timestamp, $hours_backward = false){

		$start = mktime(0,0,0, date('n'), date('j'), date('Y'));
		$end = $start + (60*60*24);
		if(is_numeric($hours_backward)){
			$start = $start - ($hours_backward*60*60);
		}
		return ($timestamp >= $start && $timestamp < $end) ? true : false;
	}
	
	
	public static function wrapAsLink($string){
		if(strstr($string, "@")){
			return "<a href=\"mailto:" . $string  . "\">" . $string . "</a>";
		}
		else {
			return "<a href=\"" . $string . "\" target=\"_blank\">" . $string . "</a>";
		}
	}
	
	
	/**
	* When using getvalues use this function to prevent manipulation 
	* when passing arguments
	* @param string $sendstring
	*/
	public static function getHash($sendString){
		$beginSecret = "9klnj80482ndl27fn2";
		$endSecret = "9sk2m!i3/nflsn";
		$toHash = $beginSecret . $sendString . $endSecret;
		return sha1($toHash);
	}
	
	/**
	* Compares the hash value with the expected hash value
	* @return bool
	*/
	public static function compareHash(){
		$string = basename($_SERVER['REQUEST_URI']);
		$string = array_shift(explode("#", $string));
		$svar = explode("&locale=", $string);
		$parts = explode("&hash=", $string);
		$beginSecret = "9klnj80482ndl27fn2";
		$endSecret = "9sk2m!i3/nflsn";
		$hash = $beginSecret . $parts[0] . $endSecret;
		$hash = sha1($hash);
		if(isset($_GET['hash']) && $_GET['hash'] == $hash){
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Sorts the array depending on it's subvalues
	 * 
	 * @param array $a
	 * @param string $subkey
	 * @param boolean $reverse
	 * @return array
	 */
	public static function subval_sort($a, $subkey, $descending = false){
		foreach($a as $k=>$v) {
			$b[$k] = strtolower($v[$subkey]);
		}
		if($descending){
			arsort($b);
		}
		else {
			asort($b);
		}
		foreach($b as $key=>$val) {
			$c[] = $a[$key];
		}
		return $c;
	}
	
	
	/**
	 * Tries to remove harmful tags from inputstring like &lt;SCRIPT&gt;, &lt;FORM&gt;, onload, onclick etc..
	 * @param type $string 
	 */
	public static function stripHarmful($string){
		$patterns = array();
		$patterns[] = "/<script.*>(.|\n)+.*<\/script>/i";
		$patterns[] = "/<form.*>(.|\n)+.*<\/form>/i";
		$patterns[] = "/\sonload=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonclick=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sondblclick=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonmouseout=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonmouseover=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonmouseout=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonunload=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonblur=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonkeyup=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonkeydown=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sondblclick=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonmousemove=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonmouseup=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonmousedown=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonabort=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonsubmit=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonfocus=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonselect=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonreset=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonchange=(\'|\").*(.*|\n).+(\'|\")/i";
		
		foreach($patterns as $pattern){
			$string = preg_replace($pattern, "", $string);
		}
		
		$string = strip_tags($string);
		return $string;

	}
}
?>