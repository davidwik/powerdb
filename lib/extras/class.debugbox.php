<?php

/**
 * The hateful debug box
 *
 * @author david
 * @package Extras
 */
class Debugbox {

	private static $_extraMsg;

	public function __construct(){
		if(Config::getInstance()->developMode){
			ThemeHelper::addMoreCSS("css/developbox.css");
			ThemeHelper::addMoreJS("js/jquery-ui.js");
			ThemeHelper::addMoreJS("js/developbox.js");
		}
	}

	public static function addMsg($string){
		if(isset($string[0])){
			if(self::$_extraMsg === NULL){
				self::$_extraMsg = "<br />Extra dbg: " . $string . "<br />";
			}
			else {
				self::$_extraMsg .= "<br />" . $string . "<br />";
			}
		}
	}

	public function getBox() {
		if(!Config::getInstance()->developMode){
			return "<!-- Debugger not active -->";
		}

$html = <<< REF
<div id="developBox">
	<div style="width: 100%; padding: 2px; font-size: 9px; text-align: right;">
	<span id="developExpand">Ðebugßox</span>
	<div id="developContents">
REF;

		
		$sessions = "Session variables used: <strong>";
		foreach($_SESSION as $key => $value){
			$sessions .= $key . ", ";
		}

		$posts = "<br />Post variables used:<br />";
		foreach($_POST as $key => $value){
			$posts .= "<strong>" . $key . "</strong> → " . $value . "<br />";	
		}

		$gets = "<br />Get's variables used:<br />";
		foreach($_GET as $key=> $value){
			$gets .= "<strong>" . $key . "</strong> → " . $value . "<br />";
		}
		$sessions = substr($sessions, 0, -2);
		$sessions .= "</strong>";
		$sessions .= "<br />";
		if(Config::getInstance()->useMySQL){
			$myNum = DB::getInstance()->getSqlRequests();
			$sql = "<br />Number of MySQL requests: ". count($myNum);
			if(count($myNum)){
				$sql .= ":";
				$o = 1;
				foreach($myNum as $t){
					$sql .= "<br />Req " . $o . ":<br /><span style=\"font-family: courier;\">" . $t . "</span>";
					$o++;
				}

				$sql .= "<br />Total database reqest time: " . round(DB::getInstance()->getTotalTime(), 4) . " seconds.";

			}
		}
		else {
			$sql = "";
		}
		
		$diff = (microtime(true) - $GLOBALS['startTime']);
		$time = "<br /><strong>Page request time: " . round($diff, 4) . " sec.</strong>";
		$html .= $sessions . $posts . $gets . self::$_extraMsg . $sql . $time .  "</div></div></div>";
		return $html;
	}
}
?>