<?php
/**
 * This is only a test wrapper for a user. 
 *
 * @author david
 * @package Soap
 */
class ExampleWSUser implements IWSUser {
	
	private $accounts; 	
	
	public function __construct(){
		if($this->accounts == null){
			$this->accounts = array(	array('admin' => 'admin'), 
										array('user' => 'user'));
		}
	}

	/**
	 * 
	 * @param string $username
	 * @param string $password
	 * @return boolean
	 */
	public function validate($username, $password){
		foreach($this->accounts as $u){
			if(key_exists($username, $u)){
				if(!strcmp($u[$username], $password)){
					return true;
				}
			}
		}
		return false;
	}
}
?>
