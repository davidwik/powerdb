<?php

/**
 * All methods returns an object with two values,
 * one called $success which can be true or false
 * @return object
 * @author David
 * @package Interfaces
 */
interface ICommand {	
	/**
	 * Executes commands specific for the form,
	 * Object should have members called success boolean, errorMsg,
	 * @param array $formData
	 * @return object
	 */
	public function execute($name, array $data);
	public function getTriggerName();
	
}
?>