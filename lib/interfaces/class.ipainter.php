<?php
/**
 * Interface for Painters
 * @author David Wiktorsson
 * @package Interfaces
 */
interface IPainter {
	/**
	 * Format can be <b>HTML</b> or <b>PDF</b> if supported
	 * @param format $format
	 */
	public function paint($format);
}?>