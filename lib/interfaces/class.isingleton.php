<?php
/**
 * The singleton interface
 * If the singleton will be used as a session saver declare session var
 * in the constructor if it doesn't exist. The destructor should just
 * update the session var if it exists. the destroy method should just remove
 * the session var.
 * @author David Wiktorsson
 * @package Interfaces
 */

interface ISingleton {
	public static function getInstance();
	public static function destroy();
}
?>
