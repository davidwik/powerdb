<?php
/**
 * @package Interfaces
 * @author David Wiktorsson
 * Interface for WSUser
 */

interface IWSUser {
	public function validate($username, $password);
}
?>