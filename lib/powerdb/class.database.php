<?php
/**
 * Database wrapper class
 *
 * @author David Wiktorsson
 * @package Base
 */

class Database implements ISingleton {
	private static $_instance = null;
	
	
	/**
	 * Database connection
	 * @var PDO
	 */
	public $connection  = null;
	
	public static function quote($string){
		return self::getInstance()->connection->quote($string, PDO::PARAM_STR);
	}

	public static function destroy() {
		self::$_instance = null;
	}
	

	public static function getColumnNames($database, $table){
		if(Config::getInstance()->databaseDriver == 'pgsql'){
			$statement = "SELECT COLUMN_NAME as col FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME=?";
			$stmt = self::getInstance()->connection->prepare($statement);
			$stmt->bindValue(1, $table);
		
		}
		else {
			$statement = "SELECT COLUMN_NAME as col FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA=? AND TABLE_NAME=?";
			$stmt = self::getInstance()->connection->prepare($statement);
			$stmt->bindValue(1, $database);
			$stmt->bindValue(2, $table);
		}

		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		$arr = array(); 
		foreach($result as $r){
			$arr[] = $r->col;
		}
		return $arr;
	}
	
	public function __construct() {
		try {
			$Config = Config::getInstance();
			$this->connection = new PDO($Config->databaseDriver . ":host=" . $Config->databaseHost . ";dbname=" . $Config->databaseName, $Config->databaseUser, $Config->databasePassword);
			$this->connection->exec('SET CHARACTER SET utf8');
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->connection->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
		} catch(PDOException $e){
			echo "ERROR: " . $e->getMessage();
			throw $e;
		}
	}
	
	public static function query($query){
		try {
			$result = self::getInstance()->connection->query($query);
			
		} catch(PDOException $e){
			throw $e;
		}
		return $result;
	}
	
	/**
	 * Returns an instance of the database handler
	 * @return Database
	 */
	public static function getInstance() {
		if(self::$_instance === null){
			self::$_instance = new self();
		}
		return self::$_instance;
	}
}
