<?php
interface IPowerModel {
	
	/**
	 * Get the object for a certain ID
	 * @param integer $id
	 */
	public function get($id);
	
	/**
	 * Returns a bool whether the table exists as an entity in the database.
	 */
	public static function tableExists();
	
	/**
	 * Updates the database table 
	 */
	public static function updateTable();
	
	
	/**
	 * Fetch a list of objects
	 * @param array statements 
	 * @return array List of items
	 */
	public function getList(array $requestArray = array(), $sortBy = false, $descending = false, $limit = false);
	
	/**
	 * Static method that will re-generate the table
	 */
	public static function generateTable();
	
	/**
	 * Saves the object. Returns the ID of the objects
	 * @return integer
	 */
	public function save();
	
	/**
	 * Saves a new copy of the object with new Primary Key
	 * @return integer
	 */
	public function saveNew();
	
	/**
	 * Returns the JSON for the object. 
	 */
	public function getJSON();
	
	/**
	 * Updates the object. 
	 * @return integer
	 */
	public function update();
}
