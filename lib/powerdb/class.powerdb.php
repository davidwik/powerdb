<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of class
 *
 * @author david
 */
class PowerDB {
	
	/**
	 * The input data
	 * @var array
	 */
	protected $data; 
	
	protected $outputString;
	
	protected $siblingOutputString;
	
	protected $objectName;
	
	protected $siblingsArr = array();
	
	protected $pkName;
	
	protected $tableName;
	
	const TABLE_PREFIX = "power_";
	
	const TYPE_POSTGRES		= 0;
	const TYPE_MYSQL		= 1; 
	const TYPE_SQLITE		= 2;
	
	
	public static function getTypes(){
		$json = file_get_contents("types.json");
		return json_decode($json);
	}
	
	public function generateFromPostData($data){
		
		// Clean up stuff that is empty
		foreach($data['property'] as $key => $value){
			if(strlen($value) <= 1){
				unset($data['property'][$key]);
				unset($data['type'][$key]);
			}
		}
		$this->data = $data;
		
		
		$this->objectName = $data['objectName'];
		$this->tableName = self::TABLE_PREFIX . strtolower($data['objectName']);
		$this->startBody(); 
		
		$this->registerSiblings();
		$this->addProperties();
		
		$this->generateConstructor();
		
		$this->generateTableString();
		
		
		$this->addGet();
		$this->addSaveNewFunction();
		$this->addUpdateFunction();
		$this->generateListFromResults();
		
		$this->addSave();
		$this->addDelete();
		
		$this->generateChildLinks();
		
		$this->generateSiblingMethods();
		
		$this->generateSaveChildren();
		$this->generateParentLinks();
		$this->generateDeleteChildrenMethods();
		
		$this->addGetListFunction();
		$this->addGetStaticListFunction();
	
		$this->fetchFromId();
		
		
		$this->addGetJSON();
		$this->addGetColumnNames();
		$this->addPropertyArray();
		
		$this->updateTable();
		
		$this->updateTableColumns();
		$this->dropTableColumns();
		$this->addTableColumns();
		
		$this->addTableExists();
	
	
		
		$this->endBody();
		
		
		
		
		
		return $this->outputString;
	}
		
	protected function registerSiblings(){
		
		foreach($this->data['type'] as $key => $val){
			if($val == "{SIBLING}"){
				$this->siblingsArr[] = array($this->objectName, $this->data['property'][$key]);
			}
		}
	}
	
	
	protected function createMapName(array $array){
		sort($array);
		$name = "";
		foreach($array as $a){
			$name .= $a;
		}
		return $name . "Map";
	}
	
	
	protected function addDelete(){
		$str = "\n\n\t/**";
		$str .= "\n\t * Deletes the object from the database";
		$str .= "\n\t * @param bool \$deep Delete all it's children";
		$str .= "\n\t * @param bool \$across Delete all SIBLING relations";
		$str .= "\n\t */";
		$str .= "\n\tpublic function delete(\$deep = false, \$across = false) {";
		$str .= "\n\t\tif(\$deep == true){";
		$str .= $this->generateDeleteChildTriggers();
		$str .= "\n\t\t}";
		$str .= $this->generateDeleteSiblingRelations();
		
		$str .= "\n\n\t\t\$statementString['mysql'] = \"DELETE FROM " . $this->tableName . " where " . strtolower($this->pkName) . "=?  LIMIT 1\";";
		$str .= "\n\t\t\$statementString['pgsql'] = \"DELETE FROM " . $this->tableName . " where " . strtolower($this->pkName) . "=?\";";
		$str .= "\n\t\t\$stmt = Database::getInstance()->connection->prepare(\$statementString[Config::getInstance()->databaseDriver]);";
		$str .= "\n\t\t\$stmt->bindValue(1, \$this->" . $this->pkName . ");";
		$str .= "\n\t\treturn \$stmt->execute();";
		$str .= "\n\t}";
		$this->outputString .= $str;
		
	}
	
	
	protected function generateDeleteSiblingRelations(){
		$str = "";
		foreach($this->data['type'] as $key => $val){
			if($val == "{SIBLING}"){
				$tableName = $this->getSiblingTableFromKey($key);
				$str .= "\n\n\t\t// Deletes the saved relations with " . $this->data['property'][$key]; 
				$str .= "\n\t\t\$statementString['pgsql'] =\"DELETE FROM " . $tableName . " WHERE " . strtolower($this->pkName) . "=?\";"; 
				$str .= "\n\t\t\$statementString['mysql'] =\"DELETE FROM " . $tableName . " WHERE " . strtolower($this->pkName) . "=?\";";
				$str .= "\n\t\t\$stmt = Database::getInstance()->connection->prepare(\$statementString[Config::getInstance()->databaseDriver]);";
				$str .= "\n\t\t\$stmt->bindValue(1, \$this->" . $this->pkName . ");";
				$str .= "\n\t\t\$stmt->execute();";
			}
		}
		return $str;
	}
	
	protected function addGet(){
		$str = "\n\n\t/**";
		$str .= "\n\t * Loads the properties from database and returns object";
		$str .= "\n\t * @param integer \$key The primary key";
		$str .= "\n\t * @return " . $this->objectName ."|null";
		$str .= "\n\t */";
		$str .= "\n\tpublic function get(\$key){";
		$str .= "\n\t\tparent::get(\$key);";
		$str .= "\n\t\t\$key = (int) \$key;";
		$str .= "\n\t\t\$result = \$this->getList(array(array('" . strtolower($this->pkName) . "', '=', \$key)), false, false, 1);";
		$str .= "\n\t\t\$obj = array_pop(\$result);";
		$str .= "\n\t\tif(!\$obj){";
		$str .= "\n\t\t\treturn null;";
		$str .= "\n\t\t}";
		
		$str .= "\n\t\tforeach(\$obj as \$k => \$v){";
		$str .= "\n\t\t\t\$this->\$k = \$v;";
		$str .= "\n\t\t}";
		$str .= "\n\t\treturn \$obj;";
		$str .= "\n\t}";
		$this->outputString .= $str;
	}


	protected function generateSiblingArrays(){
		$str = "";
		foreach($this->data['type'] as $key => $val){
			if($val == "{SIBLING}"){
				$str .= $this->addSiblingArray($key);
			}
		}
		$this->outputString .= $str;
		
	}
		
	protected function addSiblingArray($key) {
		$str = "\n\n\t/**";
		$str .= "\n\t * Holding the sibling relations";
		$str .= "\n\t * @var array";
		$str .= "\n\t */";
		$str .= "\n\tprotected $" . $this->getVarName($key) . "List;";
		return $str;
	}
	
	
	protected function addPropertyArray(){
		$str = "\n\n\t/**";
		$str .= "\n\t * Returns a list of properties for table updates";
		$str .= "\n\t * @return array";
		$str .= "\n\t */";
		$str .= "\n\tprotected static function getPropertyArray(){";
		$str .= "\n\t\t\$properties = array();";
		
		foreach($this->data['property'] as $key => $value){
			
			switch($this->getType(self::TYPE_MYSQL, $this->data['type'][$key])){
				case "{CHILD}": 
					break;
				case "{SIBLING}": 
					break;
				default:
					$str .= "\n\n\t\t\$col = new stdClass();";
					$str .= "\n\t\t\$col->name = '" . $this->getDBName($key) . "';";
					$str .= "\n\t\t\$col->datatype = array();";
					$str .= "\n\t\t\$col->datatype['mysql'] = '" . $this->getType(self::TYPE_MYSQL, $this->data['type'][$key]) . "';";
					$str .= "\n\t\t\$col->datatype['pgsql'] = '" . $this->getType(self::TYPE_POSTGRES, $this->data['type'][$key]) . "';";
					$str .= "\n\t\t\$properties[] = \$col;";	
			}
		}
			
	
		$str .= "\n\n\t\treturn \$properties;";
		$str .= "\n\t\t";
		$str .= "\n\t}";
		
		$this->outputString .= $str;
		
	}
	
	protected function updateTable(){
		
		$str = "\n\n\t/**";
		$str .= "\n\t *";
		$str .= "\n\t */";
		$str .= "\n\tfinal public static function updateTable(){";
		
		$str .= "\n\t\t\$dbColNames = self::getColumnNames();";
		$str .= "\n\t\t\$pkKey = array_search('" . strtolower($this->pkName) . "', \$dbColNames);";
		$str .= "\n\t\tunset(\$dbColNames[\$pkKey]);";
		
		$str .= "\n\t\t\$properties = self::getPropertyArray();";
		
		$str .= "\n\t\t\$newProperties = array();";
		$str .= "\n\t\t\$updateProperties = array();";
		$str .= "\n\t\t\$newNameList = array();";
		$str .= "\n\t\t\$dropProperties = array();";
		
		$str .= "\n\t\tforeach(\$properties as \$p){";
		$str .= "\n\t\t\tif(in_array(\$p->name, \$dbColNames)){";
		$str .= "\n\t\t\t\t\$updateProperties[] = \$p;";
		$str .= "\n\t\t\t}";
		$str .= "\n\t\t\telse {";
		$str .= "\n\t\t\t\t\$newProperties[] = \$p;";
		$str .= "\n\t\t\t}";
		$str .= "\n\t\t\t\$newNameList[] = \$p->name;";
		$str .= "\n\t\t}";
		
		$str .= "\n\n\t\tforeach(\$dbColNames as \$c){";
		$str .= "\n\t\t\tif(!in_array(\$c, \$newNameList)){";
		$str .= "\n\t\t\t\t\$dropProperties[] = \$c;";
		$str .= "\n\t\t\t}";
		$str .= "\n\t\t}";
		$str .= "\n\n\t\tself::updateColumns(\$updateProperties);";
		$str .= "\n\t\tself::addColumns(\$newProperties);";
		$str .= "\n\t\tself::dropColumns(\$dropProperties);";
		$str .= "\n\t}";
		$this->outputString .= $str;
		
	}
	
	protected function updateTableColumns(){
		$str = "\n\n\t/**";
		$str .= "\n\t *";
		$str .= "\n\t */";
		$str .= "\n\tprotected static function updateColumns(array \$list){";
		$str .= "\n\t\tforeach(\$list as \$l){";
		$str .= "\n\t\t\ttry {";
		$str .= "\n\t\t\t\t\$statement['mysql'] = \"ALTER TABLE " . $this->tableName . " CHANGE \" . \$l->name . \" \" . \$l->name . \" \" . \$l->datatype[Config::getInstance()->databaseDriver];";
		$str .= "\n\t\t\t\t\$statement['pgsql'] = \"ALTER TABLE " . $this->tableName . " ALTER COLUMN \" . \$l->name . \" TYPE \" . \$l->datatype[Config::getInstance()->databaseDriver];";
		$str .= "\n\t\t\t\t\$stmt = Database::getInstance()->connection->prepare(\$statement[Config::getInstance()->databaseDriver]);";
		$str .= "\n\t\t\t\t\$stmt->execute();";
		
		$str .= "\n\t\t\t}";
		$str .= "\n\t\t\tcatch(PDOException \$e){";
		$str .= "\n\t\t\t\tthrow \$e;";
		$str .= "\n\t\t\t}";
		
		$str .= "\n\t\t}";
		$str .="\n\t}";
		$this->outputString .= $str;
	}
	
	protected function addTableColumns(){
		$str = "\n\n\t/**";
		$str .= "\n\t *";
		$str .= "\n\t */";
		$str .= "\n\tprotected static function addColumns(array \$list){";
		$str .= "\n\t\tforeach(\$list as \$l){";
		$str .= "\n\t\t\ttry {";
		$str .= "\n\t\t\t\t\$statement['mysql'] = \"ALTER TABLE " . $this->tableName . " ADD \" . \$l->name  . \" \" . \$l->datatype[Config::getInstance()->databaseDriver];";
		$str .= "\n\t\t\t\t\$statement['pgsql'] = \"ALTER TABLE " . $this->tableName . " ADD COLUMN \" . \$l->name  . \" \" . \$l->datatype[Config::getInstance()->databaseDriver];";
		$str .= "\n\t\t\t\t\$stmt = Database::getInstance()->connection->prepare(\$statement[Config::getInstance()->databaseDriver]);";
		$str .= "\n\t\t\t\t\$stmt->execute();";
		$str .= "\n\t\t\t}";
		$str .= "\n\t\t\tcatch(PDOException \$e){";
		$str .= "\n\t\t\t\tthrow \$e;";
		$str .= "\n\t\t\t}";
		$str .= "\n\t\t}";
		$str .="\n\t}";
		$this->outputString .= $str;
	}

	protected function dropTableColumns(){
		$str = "\n\n\t/**";
		$str .= "\n\t *";
		$str .= "\n\t */";
		$str .= "\n\tprotected static function dropColumns(array \$list){";
		$str .= "\n\t\tforeach(\$list as \$l){";
		$str .= "\n\t\t\ttry {";
		$str .= "\n\t\t\t\t\$statement['mysql'] = \"ALTER TABLE " . $this->tableName . " DROP \" . \$l;";
		$str .= "\n\t\t\t\t\$statement['pgsql'] = \"ALTER TABLE " . $this->tableName . " DROP COLUMN \" . \$l . \" RESTRICT\";";
		$str .= "\n\t\t\t\t\$stmt = Database::getInstance()->connection->prepare(\$statement[Config::getInstance()->databaseDriver]);";
		$str .= "\n\t\t\t\t\$stmt->execute();";
		$str .= "\n\t\t\t}";
		$str .= "\n\t\t\tcatch(PDOException \$e){";
		$str .= "\n\t\t\t\tthrow \$e;";
		$str .= "\n\t\t\t}";
		$str .= "\n\t\t}";
		$str .="\n\t}";
		$this->outputString .= $str;
		
	}

	
	/**
	 * 
	 */
	protected function addProperties(){
		
		$objName = $this->data['objectName'];
		$this->objectName = $objName;
		$objName[0] = strtolower($objName[0]);
		$objName .= "Id";
		$this->pkName = $objName;
		
		$encodedData = urlencode(base64_encode(gzcompress(json_encode($this->data),9)));
		
		$this->outputString .= "\n\n\t/**";
		$this->outputString .= "\n\t * PowerDB object link";
		$this->outputString .= "\n\t * @var string";
		$this->outputString .= "\n\t */";
		$this->outputString .= "\n\tprivate \$objectURL = "; 
		if($_SERVER['SERVER_PORT'] != "80"){
			$this->outputString .= "\"http://" . $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'] . "/" . Router::getInstance()->property()->area . "?data=" . $encodedData . "\";";
		}
		else {
			$this->outputString .= "\"http://" . $_SERVER['SERVER_NAME'] . "/" . Router::getInstance()->property()->area . "?data=" . $encodedData . "\";";
		}
		$this->outputString .= "\n\n\t/**";
		$this->outputString .= "\n\t * " . $objName . " - This model's primary key";
		$this->outputString .= "\n\t * @var integer";
		$this->outputString .= "\n\t */";
		$this->outputString .= "\n\tpublic $" .  $objName . ";";
		
		$this->generateChildArrays();
		
		$this->generateSiblingArrays();
		
			
		foreach($this->data['property'] as $key => $val){
			if($this->data['property'][$key]){
				if($this->checkIfInclude($key)){
					$this->outputString .= "\n\n\t/**";
					$this->outputString .= "\n\t * " . $this->data['property'][$key];
					$this->outputString .= "\n\t * @var " . self::getPHPType($this->data['type'][$key]) . "";
					$this->outputString .= "\n\t */";
					$t = self::getPHPType($this->data['type'][$key]);
					$this->outputString .= "\n\tpublic " .  $this->getVarName($key, false);
					if($t == "integer"){
						$this->outputString .= " = 0";
					}
					else if($t == "float"){
						$this->outputString .= " = 0.0";
					}
					$this->outputString .= ";";
				}
			}
		}
	}
	
	protected function generateChildArrays(){
		foreach($this->data['type'] as $key => $val){
			if($val == "{CHILD}"){
				$this->addChildArray($key);
			}
		}
	}
	
	protected function addChildArray($key){
		$objName = $this->data['property'][$key];
		$arrName = $this->getArrName($key);
		
		$str = "\n\n\t/**";
		$str .= "\n\t * An array of " . $objName . "s";
		$str .= "\n\t * @var array " . $arrName;
		$str .= "\n\t */";
		$str .= "\n\tprotected $" . $arrName . " = array();";
		
		$this->outputString .= $str;
	}

	protected function generateDeleteChildrenMethods(){
		foreach($this->data['type'] as $key => $val){
			if($val == "{CHILD}"){
				$this->addDeleteChildren($key);
			}
		}
	}
	
	
	protected function addDeleteChildren($key){
		$objName = $this->data['property'][$key];
				
		$str = "\n\n\t/**";
		$str .= "\n\t * Deletes in the list of " . $objName . " children.";
		$str .= "\n\t */";
		$str .= "\n\tpublic function delete" . $objName . "List(array \$requestArray, \$deep = false) {";
		$str .= "\n\t\t\$requestArray[]  = array('" . strtolower($this->pkName) . "', '=', \$this->" . $this->pkName . ");";
		$str .= "\n\t\t\$objList = \$this->get" . $objName . "List(\$requestArray);";
		$str .= "\n\n\t\tforeach(\$objList as $" . $objName  .") {";
		$str .= "\n\t\t\t\$" . $objName . "->delete(\$deep);";
		$str .= "\n\t\t}";
		
		$str .= "\n\t\t"; 
		$str .= "\n\t}";
		$this->outputString .= $str;
	}
	
	protected function createSiblingTables(){
		$str = "";
		foreach($this->siblingsArr as $array){
			
			// MySQL
			$tableName = strtolower(self::TABLE_PREFIX . $this->createMapName($array));
			$str .= "\n\n\t\t\$sql['mysql'][] = \"DROP TABLE " . $tableName . "\";";
			$str .= "\n\t\t\$sql['mysql'][] = \"CREATE TABLE " . $tableName . " ("; 
			foreach($array as $a){
				$str .= strtolower($a) . "id INT UNSIGNED, ";
			}
			$str = substr($str, 0,-2);
			$str .= ", PRIMARY KEY(";// ;((
			
			foreach($array as $a){
				$str .= strtolower($a) . "id,";
			}
			$str = substr($str, 0, -1) . "))\";";
						
			// PostGRE
			$str .= "\n\n\t\t\$sql['pgsql'][] = \"DROP TABLE " . $tableName . "\";";
			$str .= "\n\t\t\$sql['pgsql'][] = \"CREATE TABLE " . $tableName . " ("; 
			foreach($array as $a){
				$str .= strtolower($a) . "id INT, ";
			}
			$str = substr($str, 0,-2);
			$str .= ", PRIMARY KEY(";// ;((
			
			foreach($array as $a){
				$str .= strtolower($a) . "id,";
			}
			$str = substr($str, 0, -1) . "))\";";
			
			
		}
		$str .= "\n";
		return $str;
		
	}
		
	/**
	 * Checks the variable name, and sees whether it should be renamed as an reference.
	 * @param int $key
	 * @return string
	 */
	protected function getVarName($key, $includeThis = false){
		$name = "";
		switch($this->data['type'][$key]){
			case "{PARENT}":
				$name = $this->data['property'][$key];
				$name[0] = strtolower($name[0]);
				$name = $name . "Id";
				break;
			case "{SIBLING}":
				$name = $this->data['property'][$key];
				$name[0] = strtolower($name[0]);
				return $name; 
				
			default: 
				$name = $this->data['property'][$key];
				break;
		}
		if($includeThis){
			return "\$this->" . $name;
		}
		else {
			return "$" . $name;
		}
	}
	
	protected function generateChildLinks(){
		foreach($this->data['type'] as $key => $val){
			if($val == "{CHILD}"){
				$this->addGetChildList($key);
				$this->addAddChild($key);
			}
		}
	}
	
	protected function addGetChildList($key){
		$objName = $this->data['property'][$key];
		$str = "\n\n\t/**";
		$str .= "\n\t * Get a list of associated " . $objName . "s";
		$str .= "\n\t * @param array \$requestArray";
		$str .= "\n\t * @param string \$sortBy";
		$str .= "\n\t * @param bool \$descending";
		$str .= "\n\t * @param integer \$limit";
		$str .= "\n\t * @return array";
		$str .= "\n\t */";
		$str .= "\n\tpublic function get" . $objName . "List(array \$requestArray = array(), \$sortBy = false, \$descending = false, \$limit = false) {";
		
		$ObjId = strtolower($this->pkName);
		
		$str .= "\n\t\tif(!\$this->" . $this->pkName . ") {";
		$str .= "\n\t\t\treturn array();";
		$str .= "\n\t\t}";
		$str .= "\n\t\t\$requestArray[] = array('" . $ObjId  ."', '=', \$this->" . $this->pkName .");";
		$str .= "\n\t\t\$" . $objName . " = new " . $objName . "();";
		$str .= "\n\t\t\$arrayList = $" . $objName ."->getList(\$requestArray, \$sortBy, \$descending, \$limit);";
		$str .= "\n\t\treturn \$arrayList;";
		$str .= "\n\t}";
		$this->outputString .= $str;
	}
	
	private function getArrName($key){
		$objName = $this->data['property'][$key];
		$arrName = $objName;
		$arrName[0] = strtolower($arrName[0]);
		$arrName .= "List";
		return $arrName;
	}
	
	protected function addAddChild($key){
		$objName = $this->data['property'][$key];
		$arrName = $this->getArrName($key);
		$str = "\n\n\t/**";
		$str .= "\n\t * Add a " . $objName; 
		$str .= "\n\t * @param " . $objName . " $" . $objName;
		$str .= "\n\t */";
		$str .= "\n\tpublic function add" . $objName . "(" . $objName . " $" . $objName .") {";
		$str .= "\n\t\t\$this->" . $arrName . "[] = $" . $objName . ";";
		$str .= "\n\t}\n";
		$this->outputString .= $str;
	}
	
	protected function generateDeleteChildTriggers(){
		$str = ""; 
		foreach($this->data['type'] as $key => $val){
			$objName = $this->data['property'][$key];
			if($val == "{CHILD}"){
				$str .= "\n\t\t\t\$this->delete" . $objName . "List(array(array('" . $this->pkName . "', '=', \$this->" . $this->pkName . ")), \$deep);";
			}
		}
		return $str;
	}
	
	protected function generateSiblingMethods(){
		foreach($this->data['type'] as $key => $val){
			if($val == "{SIBLING}"){
				$this->outputString .= $this->addSiblingMethod($key);
				$this->outputString .= $this->getSiblingList($key);
				$this->outputString .= $this->addRemoveSiblingLink($key);
			}
		}
	}
	
	
	protected function getSiblingList($key){
		$targetTable = strtolower(self::TABLE_PREFIX . $this->data['property'][$key]);
		$mapTable = $this->getSiblingTableFromKey($key);
		$accessor = $this->getDBName($key);
		$str = "\n\n\t/**";
		$str .= "\n\t * Gets a list of " . $this->data['property'][$key];
		$str .= "\n\t * @param array \$requestArray";
		$str .= "\n\t * @param string \$sortBy";
		$str .= "\n\t * @param bool \$descending";
		$str .= "\n\t * @param integer \$limit";
		$str .= "\n\t * @return array";
		$str .= "\n\t */";
		$str .= "\n\tpublic function get" . $this->data['property'][$key] . "List(array \$requestArray = array(), \$sortBy = false, \$descending = false, \$limit = false){";
		$str .= "\n\t\tif(!\$this->" . $this->pkName . "){";
		$str .= "\n\t\t\treturn array();";
		$str .= "\n\t\t}";
		
		$str .= "\n\t\t\$statement = \"SELECT " . $targetTable . ".* FROM " . $mapTable . " LEFT JOIN " . $targetTable . " ON (" . $mapTable . "." . $accessor . "=" . $targetTable . "." . $accessor . ")\";";
		$str .= "\n\t\t\$requestArray[] = array('" . $mapTable . "." . strtolower($this->pkName) . "', '=', \$this->" . $this->pkName . ");";
		$str .= "\n\t\t\$statement .= \$this->appendConditions(\$requestArray, \$sortBy, \$descending, \$limit, \"" . $targetTable . "\");";
		
		$str .= "\n\t\t\$stmt = Database::getInstance()->connection->prepare(\$statement);";
		$str .= "\n\t\t\$cnt = 1;";
		$str .= "\n\t\tforeach(\$this->getBindArray() as \$bind){";
		$str .= "\n\t\t\t\$stmt->bindValue(\$cnt, \$bind);";
		$str .= "\n\t\t\t\$cnt++;";
		$str .= "\n\t\t}";
		$str .= "\n\t\t\$stmt->execute();";
		$str .= "\n\t\t\$res = \$stmt->fetchAll(PDO::FETCH_OBJ);";
		$str .= "\n\t\treturn " . $this->data['property'][$key] . "::createListFromResults(\$res);";
		$str .= "\n\t}";
		return $str;
	}
	
	protected function generateListFromResults(){
		$str = "\n\n\t/**";
		$str .= "\n\t * Returns a list of " . $this->objectName . " built from a list of responses.";
		$str .= "\n\t * @param array";
		$str .= "\n\t */";
		$str .= "\n\tpublic static function createListFromResults(array \$list){";
		$str .= "\n\t\t\$arrayList = array();";
		$str .= "\n\t\tforeach(\$list as \$row){";
		$str .= "\n\n\t\t\t\$o = new " . $this->objectName . "();";
		$str .= "\n\t\t\t\$o->" . $this->pkName . " = \$row->" . strtolower($this->pkName) . ";";
		foreach($this->data['property'] as $key => $val) {
			if($this->checkIfInclude($key)) {
				$str .= "\n\t\t\t\$o->" . substr($this->getVarName($key), 1) . " = \$row->" . $this->getDBName($key) . ";";
			}
		}
		$str .= "\n\t\t\t\$arrayList[] = \$o;";
		$str .= "\n\t\t}";
		$str .= "\n\t\treturn \$arrayList;";
		$str .= "\n\t}";
		$this->outputString .= $str;
		
		
	}
	
	protected function addSiblingMethod($key){
		$objectName = $this->data['property'][$key];
		$str = "\n\n\t/**";
		$str .= "\n\t * Adds a many-to-many relation with an " . $objectName . " object.";
		$str .= "\n\t * @param " . $objectName;
		$str .= "\n\t */";
		$str .= "\n\tpublic function add" . $objectName . "(" . $objectName . " $" . $objectName . ") {";
		$str .= "\n\t\t\$this->" . $this->getVarName($key, true) . "List[] = $" . $objectName . ";";
		$str .= "\n\t}";
		return $str;
	}
	
	protected function getSiblingTableFromKey($key){
		$objectName = $this->data['property'][$key];
		
		foreach($this->siblingsArr as $arr){
			if(in_array($objectName, $arr)){
				return strtolower(self::TABLE_PREFIX . $this->createMapName($arr));
			}
		}
		
	}
	
	protected function addTableExists(){
		$str = "\n\n\t/**"; 
		$str .= "\n\t * Checks whether a table exists in the database";
		$str .= "\n\t * @return boolean";
		$str .= "\n\t */";
		$str .= "\n\tfinal public static function tableExists(){";
		$str .= "\n\t\t$" . $this->objectName . " = new " . $this->objectName . "();";
		$str .= "\n\t\treturn $" . $this->objectName . "->checkTableExists(\"" . $this->tableName . "\");";
		$str .= "\n\t}";
		$this->outputString .= $str;
	}
	

	protected function addRemoveSiblingLink($key){
		$objectName = $this->data['property'][$key];
		$tableName = $this->getSiblingTableFromKey($key);
		
		$str = "\n\n\t/**";
		$str .= "\n\t * Removes a many-to-many relation with a passed " . $objectName . " object.";
		$str .= "\n\t * @param " . $objectName;
		$str .= "\n\t */";
		$str .= "\n\tpublic function remove" . $objectName . "Map(" . $objectName . " $" . $objectName .") {";
		$str .= "\n\t\tif($" . $objectName . "->" . $this->getVarName($key) . "Id && \$this->" . $this->pkName .") {";
		$str .= "\n\t\t\t\$statementString = \"DELETE FROM " . $tableName ." WHERE " . $this->getDBName($key) . "=? AND ". strtolower($this->pkName) ."=?\";";
		$str .= "\n\t\t\t\$stmt = Database::getInstance()->connection->prepare(\$statementString);";
		$str .= "\n\t\t\t\$stmt->bindValue(1, \$" . $objectName ."->" . $this->getVarName($key) ."Id);";
		$str .= "\n\t\t\t\$stmt->bindValue(2, \$this->" . $this->pkName . ");";
		$str .= "\n\t\t\treturn \$stmt->execute();";
		
		$str .= "\n\t\t}";
		$str .= "\n\t\telse {";
		$str .= "\n\t\t\tthrow new ErrorException(\"Failed to get the Id from the object " . $objectName ." or from the current object\");";
		$str .= "\n\t\t}";
		$str .= "\n\t\t"; 
		$str .= "\n\t}";
		return $str;
		
	}
	
	protected function generateParentLinks(){
		foreach($this->data['type'] as $key => $val){
			if($val == "{PARENT}"){
				$this->addGetParent($key);
				$this->addSetParent($key);
			}
		}
	}

	
	protected function generateSaveChildren(){
		foreach($this->data['type'] as $key => $val){
			if($val == "{CHILD}"){
				$this->addSaveChildren($key);
			}
		}
	}
	
	protected function generateSaveChildrenTriggers(){
		$str = "";
		foreach($this->data['type'] as $key => $val){
			if($val == "{CHILD}"){
				$str .= $this->addSaveChildrenTrigger($key);
			}
		}
		return $str;
	}
	
	
	protected function addSaveChildrenTrigger($key){
		$objName = $this->data['property'][$key];
		$str = "\n\t\t\$this->save" . $objName . "List();";
		return $str;
	}

	protected function addSaveChildren($key){
		$objName = $this->data['property'][$key];
		$arrName = $this->getArrName($key);		
		$str = "\n\n\t/**";
		$str .= "\n\t * Saves all the related children of " . $objName ." and refer them to current object.";
		$str .= "\n\t */";
		$str .= "\n\tprotected function save" . $objName . "List() {";
		$str .= "\n\t\tforeach(\$this->" . $arrName . " as $" . $objName . ") {";
		$str .= "\n\t\t\t$" . $objName . "->" . $this->pkName . " = \$this->"  .$this->pkName . ";";
		$str .= "\n\t\t\t$" . $objName ."->save();";
		$str .= "\n\t\t}";
		$str .= "\n\t}";
		$this->outputString .= $str;
	}
	
	protected function generateSaveSiblingsTriggers(){
		$str = "";

		foreach($this->data['type'] as $key => $val){
			if($val == "{SIBLING}"){
				$str .= $this->addSaveSibling($key);
			}
		}
		return $str;
	}
	
	
	protected function addSaveSibling($key){
		
		$tablename = $this->getSiblingTableFromKey($key);
		$varName = $this->data['property'][$key];
		
		
		
		$str = "\n\t\tif(\$this->" . $this->getVarName($key, true) . "List){";
		$str .= "\n\t\t\tforeach(\$this->" . $this->getVarName($key, true) . "List as \$" . $this->data['property'][$key] . "){";
		
		$str .= "\n\t\t\t\tif(!\$" . $varName . "->" . $this->getVarName($key) . "Id" . "){";
		$str .= "\n\t\t\t\t\t\$" . $varName . "->save();";
		$str .= "\n\t\t\t\t}";
		$str .= "\n\t\t\t\t\$result = \$this->get" . $this->data['property'][$key] . "List(array(array('" . $this->getDBName($key) . "', '=', $" . $this->data['property'][$key] . "->" . $this->getVarName($key)  . "Id)), false, false, 1);";
		$str .= "\n\n\t\t\t\t// If sibling already save, do not add!";
		$str .= "\n\t\t\t\tif(!count(\$result)){";
	
		$str .= "\n\t\t\t\t\t\$statement = \"INSERT INTO " . $tablename ." (" . $this->getDBName($key) . ", " . strtolower($this->pkName) . ") VALUES (?, ?)\";";
			
		$str .= "\n\t\t\t\t\t\$stmt = Database::getInstance()->connection->prepare(\$statement);";
		$str .= "\n\t\t\t\t\t\$stmt->bindValue(1, \$" . $varName . "->" . $this->getVarName($key) . "Id);";
		$str .= "\n\t\t\t\t\t\$stmt->bindValue(2, \$this->" . $this->pkName . ");";
		$str .= "\n\t\t\t\t\t\$stmt->execute();";
		$str .= "\n\t\t\t\t}";
		$str .= "\n\t\t\t}";
		$str .= "\n\t\t\t\$this->" . $this->getVarName($key, true) . "List = array();";
		$str .= "\n\t\t}";
		return $str;
	}
	
	
	
	
	public static function getPHPType($type){
		if(preg_match("/varchar/i", $type)){
			return "string";
		}
		else {
			switch($type){
				case "INT":
					return "integer";
					break;
				case "BIGINT": 
					return "integer";
					break;
				case "TINYINT":
					return "integer";
					break;
				case "DECIMAL": 
					return "float";
					break;
				case "TEXT":
					return "string";
					break;
				case "{CHILD}":
					return "array";
					break;
				case "{PARENT}":
					return "integer";
					break;
				
				default:
					return "type";
			}
		}
	}
	
	
	protected function generateConstructor(){
		$non = "";
		$str = "\n\n\t/**"; 
		$str .= "\n\t * Instantiates a new " . $this->objectName;
		
		$str .= "\n\t *";
		$hasProp = false;
		foreach($this->data['property'] as $key => $val){
			if($this->data['property'][$key]){
				if($this->checkIfInclude($key)){
					$t = self::getPHPType($this->data['type'][$key]);
					$str .= "\n\t * @param "; 
					$str .= $t . " ";
					$str .= $this->getVarName($key);
					$hasProp = true;
				}
			}
		}
		
		if(!$hasProp){
			$non = "\n\n\t/**"; 
			$non .= "\n\t * Instantiates a new " . $this->objectName;
			$non .= "\n\t */";
			$non .= "\n\tpublic function __construct() {}";
		}
		
		
		$str .= "\n\t */";
		$str .= "\n\tpublic function __construct(";
		
		
		foreach($this->data['property'] as $key => $val){
			if($this->data['property'][$key]){
				if($this->checkIfInclude($key)){
					$str .= $this->getVarName($key);
					$t = self::getPHPType($this->data['type'][$key]);
					$str .= " = " . $this->getDefaultValue($key) . ", ";
				}
			}
		}
		$str = substr($str, 0, -2);
		$str .= ") {";
		$str .= "\n\t\t";
		
		foreach($this->data['property'] as $key => $val){
			if($this->data['property'][$key]){
				if($this->checkIfInclude($key)){
					$t = self::getPHPType($this->data['type'][$key]);
					$str .= "\n\t\t" . $this->getVarName($key, true);
					$str .= " = (" . $this->getVarName($key) . ") ? " . $this->getVarName($key) . " : " . $this->getDefaultValue($key) . ";";
					
				}
			}
		}
		
		
		$str .= "\n\t}";
		
		
		if($hasProp){
			$this->outputString .= $str;
		}
		else {
			$this->outputString .= $non;
		}
		
	}
	
	
	protected function getDefaultValue($key){
		$res = $this->getPHPType($this->data['type'][$key]);
		switch($this->getPHPType($this->data['type'][$key])){
			case "string":
				return "\"\"";
				break;
			case "integer": 
				return 0;
				break;
			case "float":
				return 0.0;
				break;

			default: 
				return "\"\"";
		}
	}
	
	protected function getDBName($key){
		
		$name = "";
		switch($this->data['type'][$key]){
			case "{PARENT}": 
				$name = $this->data['property'][$key]; 
				$name = strtolower($name) . "id";
				break;
			case "{SIBLING}":
				$name = $this->data['property'][$key];
				$name = strtolower($name) . "id";
				break;
			default: 
				$name = strtolower($this->data['property'][$key]);
				break;
		}
		return $name;
	}
		
	
	protected function generateTableString(){
		$str = "\n\n\t/**";
		$str .= "\n\t *";
		$str .= "\n\t * Generates the object table for the appropriate database driver";
		$str .= "\n\t */";
		$str .= "\n\tfinal public static function generateTable() {";
			
		// MYSQL 
		$str .= "\n\t\t\$sql = array();";
		$str .= "\n\t\t\$sql['mysql'][] = \"DROP TABLE " . self::TABLE_PREFIX . strtolower($this->objectName) . "\";";
		$str .= "\n\t\t\$sql['mysql'][] = \"";
		$str .= "CREATE TABLE " . self::TABLE_PREFIX . strtolower($this->objectName) . " (" . strtolower($this->pkName) . " INT UNSIGNED NOT NULL AUTO_INCREMENT, PRIMARY KEY(" . strtolower($this->pkName) . "), ";
		foreach($this->data['property'] as $key => $val){
			if($this->checkIfInclude($key)){
				$str .= $this->getDBName($key) . " " . self::getType(self::TYPE_MYSQL, $this->data['type'][$key]) . ", "; 
			}
		
		}
		$str = substr($str, 0, -2);
		
		$str .= ")\";";
		$str .= $this->addIndex(self::TYPE_MYSQL);
		//POSTGRES 
		$str .= "\n\n\t\t\$sql['pgsql'][] = \"DROP TABLE " . self::TABLE_PREFIX . strtolower($this->objectName) . "\";";
		$str .= "\n\t\t\$sql['pgsql'][] = \"";
		$str .= "CREATE TABLE " . self::TABLE_PREFIX . strtolower($this->objectName) . " (" . strtolower($this->pkName) . " SERIAL PRIMARY KEY, ";
		
		foreach($this->data['property'] as $key => $val){
			if($this->checkIfInclude($key)){
				$str .= $this->getDBName($key) . " " . self::getType(self::TYPE_POSTGRES, $this->data['type'][$key]) . ", "; 
			}
		}
		
		$str = substr($str, 0, -2);
		$str .= ")\";";
		$str .= $this->addIndex(self::TYPE_POSTGRES);
		
		$str .= $this->createSiblingTables();
		
		
		$str .= "\n\t\t\$createTable = \$sql[Config::getInstance()->databaseDriver];";
	
		$str .= "\n\n\t\tforeach(\$createTable as \$c){";
		$str .= "\n\t\t\ttry {";
		$str .= "\n\t\t\t\tDatabase::query(\$c);";
		$str .= "\n\t\t\t} catch(PDOException \$e) {";
		
		$str .= "\n\t\t\t\t; // Do nothing since, the table be created or not existing.";
		$str .= "\n\t\t\t}";
		$str .= "\n\t\t}";
		
		
		$str .= "\n\t}";
		
		
		$this->outputString .= $str;
	}
	
	public function addSaveNewFunction(){
		
		$str = "\n\n\tpublic function saveNew() {";
		$str .= "\n\t\t\$statementString = \"INSERT INTO " . self::TABLE_PREFIX . strtolower($this->objectName) . " (";
		foreach($this->data['property'] as $key => $p){
			if($p){
				if($this->checkIfInclude($key)){
					$str .= $this->getDBName($key) . ", ";
				}
			}
		}
		$str = substr($str, 0, -2) . ") VALUES (";
		
		foreach($this->data['property'] as $key => $p){
			if($this->checkIfInclude($key)){
				$str .= "?, ";
			}
		}
		$str = substr($str, 0, -2) . ")\";";
		
		$str .= "\n\t\t\$stmt = Database::getInstance()->connection->prepare(\$statementString);";
		$cnt = 1;
		foreach($this->data['property'] as $key => $val){
			if($this->checkIfInclude($key)){
				$str .= "\n\t\t\$stmt->bindValue(" . $cnt . ", " . $this->getVarName($key, true) . ");";
				$cnt++;
			}
		}
		$str .= "\n\t\t\$stmt->execute();";
		$str .= "\n\t\t\$pk = \$this->getLastId(\"" . strtolower($this->tableName) . "\",\"" . strtolower($this->pkName) . "\");";
				
		$str .= "\n\t\t\$this->" . $this->pkName . " = \$pk;";
		
		$str .= $this->generateSaveChildrenTriggers();
		$str .= $this->generateSaveSiblingsTriggers();
		$str .= "\n\t\treturn \$this->" . $this->pkName .";";
		$str .= "\n\t}\n";
		$this->outputString .= $str;

	}
	
	
	public function addGetColumnNames(){
		$str = "\n\n\t/**";
		$str .= "\n\t * Returns the column names of the table in an array.";
		$str .= "\n\t * @return array";
		$str .= "\n\t */";
		$str .= "\n\tpublic static function getColumnNames(){";
		$str .= "\n\t\treturn Database::getColumnNames(Config::getInstance()->databaseName, \"" . $this->tableName . "\");";
		$str .= "\n\t}";
		$this->outputString .= $str;
	}
	
	
	public function addUpdateFunction(){
		$str = "\n\n\tpublic function update() {";
		$str .= "\n\t\t\$statementString = \"UPDATE " . self::TABLE_PREFIX . strtolower($this->objectName) . " SET ";
		foreach($this->data['property'] as $key => $p){
			if($p){
				if($this->checkIfInclude($key)){
					$str .= $this->getDBName($key) . "=?, "; 
				}
			}
		}
		$str = substr($str, 0, -2);
		$str .= " WHERE " . strtolower($this->pkName) . "=? LIMIT 1\";";
		
		$str .= "\n\t\tif(Config::getInstance()->databaseDriver == \"pgsql\"){";
		$str .= "\n\t\t\t\$statementString = str_replace(\"LIMIT 1\", \"\", \$statementString);"; 
		$str .= "\n\t\t}";
		
		$str .= "\n\t\t\$stmt = Database::getInstance()->connection->prepare(\$statementString);";
		
		
		
		$cnt = 1;
		foreach($this->data['property'] as $key => $val){
			if($val){
				if($this->checkIfInclude($key)){
					$str .= "\n\t\t\$stmt->bindValue(" . $cnt . ", " . $this->getVarName($key, true) . ");";
					$cnt++;
				}
			}
		}
		
		$str .= "\n\t\t\$stmt->bindValue(" . $cnt . ", \$this->" . $this->pkName . ");";
		$str .= "\n\t\t\$stmt->execute();";
		
		$str .= $this->generateSaveChildrenTriggers();
		$str .= $this->generateSaveSiblingsTriggers();
		$str .= "\n\t}\n";
		$this->outputString .= $str;
	}
	
	protected function addIndex($DB_TYPE){
		$returnString = null;
		if($DB_TYPE == self::TYPE_MYSQL){
			foreach($this->data['property'] as $key => $value){
				if($this->data['type'][$key] == "{PARENT}"){
					$returnString .= "\n\t\t\$sql['mysql'][] = \"CREATE INDEX " . $this->getDBName($key) . " ON " . self::TABLE_PREFIX . strtolower($this->objectName) . " (" . $this->getDBName($key) . ")\";";
				}
			}
		}
		else if($DB_TYPE == self::TYPE_POSTGRES) {
			foreach($this->data['property'] as $key => $value){
				if($this->data['type'][$key] == "{PARENT}"){
					$returnString .= "\n\t\t\$sql['pgsql'][] = \"CREATE INDEX " . $this->getDBName($key) . " ON " . self::TABLE_PREFIX . strtolower($this->objectName) . " (" . $this->getDBName($key) . ")\";";
				}
			}
		}
		return $returnString;
	
	}
	
	public function checkIfInclude($key){
		$type = $this->data['type'][$key];
		if($type == "{SIBLING}" || $type == "{CHILD}"){
			return false;
		}
		else {
			return true;
		}
	}
	
	
	public static function getType($DB_TYPE, $type){
		
		// FOR MYSQL 
		if($DB_TYPE == self::TYPE_MYSQL){
			if($type == "{PARENT}"){
				return "INT";
			}
			return $type;
		}
		// FOR POSTGRESQL
		else if($DB_TYPE == self::TYPE_POSTGRES){
			
			if($type == "TINYINT"){
				return "SMALLINT";
			}
			else if($type == "{PARENT}"){
				return "INT";
			}
			else {
				return $type;
			}
		}
		// FOR SQLITE
		else if($DB_TYPE == self::TYPE_SQLITE){
			if(preg_match("/int/i", $type)){
				return "INTEGER";
			}
			else if($type == "FLOAT"){
				return "REAL";
			}
			else {
				return $type;
			}
		}
		else {
			return $type;
		}
	}
	
	
	protected function addGetListFunction(){
		$str = "\n\n\t/**";
		$str .= "\n\t * Get a list of " . $this->objectName . "s";
		$str .= "\n\t * @param array \$requestArray";
		$str .= "\n\t * @param string \$sortBy";
		$str .= "\n\t * @param bool \$descending";
		$str .= "\n\t * @param integer \$limit";
		$str .= "\n\t * @return array";;
		$str .= "\n\t */";
		
		$str .= "\n\tpublic function getList(array \$requestArray = array(), \$sortBy = false, \$descending = false, \$limit = false) {";
		$str .= "\n\t\t\$statementString =\"SELECT * from " . $this->tableName . "\";";
		$str .= "\n\t\t\$statementString .= \$this->appendConditions(\$requestArray, \$sortBy, \$descending, \$limit);";
		
		
		$str .= "\n\t\t\$stmt = Database::getInstance()->connection->prepare(\$statementString);";
		$str .= "\n\t\t\$cnt = 1;";
		$str .= "\n\t\tforeach(\$this->getBindArray() as \$bind){";
		$str .= "\n\t\t\t\$stmt->bindValue(\$cnt, \$bind);";
		$str .= "\n\t\t\t\$cnt++;";
		$str .= "\n\t\t}";
		
		$str .= "\n\t\t\$stmt->execute();";
		$str .= "\n\t\t\$result = \$stmt->fetchAll(PDO::FETCH_OBJ);";
		
		$str .= "\n\n\t\t\$arrayList = array();";
		$str .= "\n\n\t\tforeach(\$result as \$row){";
		$str .= "\n\n\t\t\t\$o = new " . $this->objectName . "();";
		$str .= "\n\t\t\t\$o->" . $this->pkName . " = \$row->" . strtolower($this->pkName) . ";";
		foreach($this->data['property'] as $key => $val) {
			if($this->checkIfInclude($key)) {
				$str .= "\n\t\t\t\$o->" . substr($this->getVarName($key), 1) . " = \$row->" . $this->getDBName($key) . ";";
			}
		}
		$str .= "\n\t\t\t\$arrayList[] = \$o;";
		$str .= "\n\t\t}";
		$str .= "\n\t\treturn \$arrayList;\n\t}";
		
		$this->outputString .= $str;
	}
	
	protected function addGetStaticListFunction(){
		$str = "\n\n\t/**";
		$str .= "\n\t * Get a list of " . $this->objectName . "s";
		$str .= "\n\t * @param array \$requestArray";
		$str .= "\n\t * @param string \$sortBy";
		$str .= "\n\t * @param bool \$descending";
		$str .= "\n\t * @param integer \$limit";
		$str .= "\n\t * @return array";
		$str .= "\n\t */";
		
		$str .= "\n\tpublic static function fetchAll(array \$requestArray = array(), \$sortBy = false, \$descending = false, \$limit = false) {";
		$str .= "\n\t\t\$obj = new " . $this->objectName . "();";
		$str .= "\n\t\t\$result = \$obj->getList(\$requestArray, \$sortBy, \$descending, \$limit);";
		$str .= "\n\t\tunset(\$obj);";
		$str .= "\n\t\treturn \$result;";
		$str .= "\n\t}";
		$this->outputString .= $str;
	}
	
	protected function fetchFromId(){
		$str = "\n\n\t/**";
		$str .= "\n\t * Returns a " . $this->objectName;
		$str .= "\n\t * @param integer $" . $this->pkName; 
		$str .= "\n\t * @return " . $this->objectName . "|null";
		$str .= "\n\t */";
		$str .= "\n\tpublic static function fetch(\$" . $this->pkName . "){";
		$str .= "\n\t\t\$obj = new " . $this->objectName . "();";
		$str .= "\n\t\t\$obj->get(\$" . $this->pkName . ");";
		$str .= "\n\t\treturn \$obj;";
		$str .= "\n\t}";
		$this->outputString .= $str;
	}
	
	protected function addGetParent($key) {
		$objName = $this->data['property'][$key];
		$tableName = self::TABLE_PREFIX . strtolower($objName);
		$pkName = strtolower($objName) . "id";
		$str = "\n\n\t/**";
		$str .= "\n\t * Returns the parent " . $objName;
		$str .= "\n\t * @return " . $objName . "|null";
		$str .= "\n\t */";
		$str .= "\n\tpublic function get" . $objName . "() {";
		$str .= "\n\t\t\$" . $objName . " = new " . $objName ."();";
		$str .= "\n\t\t\$result = \$" . $objName . "->getList(array(array('" . $this->getDBName($key) . "', '=', " . $this->getVarName($key, true) . ")), false, false, 1);";
		$str .= "\n\t\treturn array_pop(\$result);";
		$str .= "\n\t}";
		$this->outputString .= $str;
	}
	
	protected function addSetParent($key) {
		$objName = $this->data['property'][$key];
		$varName = $this->getVarName($key, true);
		$oName = $this->getVarName($key);
		
		$str = "\n\n\t/**";
		$str .= "\n\t * Sets reference to parent " . $objName; 
		$str .= "\n\t */";
		$str .= "\n\tpublic function set" . $objName . "(" . $objName . " $" . $objName .") {";
		
		$str .= "\n\t\tif(!$" . $objName ."->" . substr($oName, 1) . "){";
		$str .= "\n\t\t\t$" . $objName . "->add" . $this->objectName . "(\$this);";
		$str .= "\n\t\t}";
		$str .= "\n\t\telse {";
		$str .= "\n\t\t\t" . $varName . " = $" . $objName . "->" . substr($oName, 1) .";";
		$str .= "\n\t\t}";
		$str .= "\n\t}";
		$this->outputString .= $str;
	}
	
	protected function addGetJSON() {
		$str = "\n\n\t/**";
		$str .= "\n\t * Returns the json response of the object";
		$str .= "\n\t */\n\tpublic function getJSON() {";
		$str .= "\n\t\treturn json_encode(\$this);";
		$str .= "\n\t}";
		$this->outputString .= $str;
	}

	
	protected function addSave() {
		$str = "\n\n\t/**"; 
		$str .= "\n\t * Saves the object to the database and return the primary key.";
		$str .= "\n\t * @return integer";
		$str .= "\n\t */";
		$str .= "\n\tpublic function save() {";
		$str .= "\n\t\tif(\$this->" . $this->pkName . ") {";
		$str .= "\n\t\t\t\$id = \$this->update();";
		$str .= "\n\t\t}";
		$str .= "\n\t\telse {";
		$str .= "\n\t\t\t\$id = \$this->saveNew();";
		$str .= "\n\t\t}";
		$str .= "\n\t\treturn \$id;";
		$str .= "\n\t}";
		
		$this->outputString .= $str; 
	}
	

	protected function startBody() {
		$str = "<?php\n/**\n * Database wrapper for " . $this->objectName;
		$str .= "\n * Do NOT edit this file. Extend this base class to implement your logic.";
		$str .= "\n * Generated on " . Utils::getDateFromTimestamp(time(), true);
		$str .= "\n * @package PowerDB";
		$str .= "\n * @author PowerDB Magic Writer";
		$str .= "\n */";
		$str .= "\nclass Base" . $this->data['objectName'] . " extends PowerModel implements IPowerModel {\n";
		$this->outputString = $str; 
	}
	
	protected function endBody() {
		$this->outputString .= "\n}";
	}
}