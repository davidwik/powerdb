<?php
/**
 * Description of class
 *
 * @author david
 */
abstract class PowerModel {
	
	private $bindArray = array();
	
	public function get($id){
		if(!is_numeric($id)){
			throw new Exception("Key is not a number!");
		}
	}
	
	/**
	 * Returns the last inserted ID
	 * @param string $tablename
	 * @return int
	 */
	protected function getLastId($tablename, $primaryKey){
		$id = 0;
		
		// PostGre needs to know which key to get..
		if(strtolower(Config::getInstance()->databaseDriver) == "pgsql"){
			$tableSeq = $tablename . "_" . $primaryKey . "_seq";
			$id = (int) Database::getInstance()->connection->lastInsertId($tableSeq);
		}
		else {
			$id = (int) Database::getInstance()->connection->lastInsertId();
		}
		
		if(is_int($id)){
			return $id;
		}
		else {
			throw new ErrorException("Failed to acquire the last primary key");
		}
	}
	
	/**
	 * Interprets the array and generate sql;
	 * @param array $array
	 * @return string
	 */
	protected function appendConditions($array, $sortBy = false, $descending = false, $limit = false, $prefixTable = false) {
		$condition = "";
		$this->bindArray = array();
		
		$prefixTable = ($prefixTable) ? $prefixTable . "." : "";
		if(isset($array)){
			if(!is_array($array)){
				throw new Exception("The result array is not an array");
			}
			if($array){
				$condition .= " WHERE ";
				foreach($array as $part){
					
					// If the part already contains a table name, do not include the prefixtable
					if(preg_match("/\./", $part[0])){
						if(Config::getInstance()->databaseDriver == "pgsql" && !is_numeric($part[2])){
							$condition .= "LOWER(" . strtolower($part[0]) . ")" . $part[1] . "LOWER(?)";
						}
						else {
							$condition .= strtolower($part[0]) . "" . $part[1] . "?";
						}
					}
					else {
						if(Config::getInstance()->databaseDriver == "pgsql" && !is_numeric($part[2])){
							$condition .= "LOWER(" . strtolower($part[0]) . ")" . $part[1] . "LOWER(?)";
						}
						else {
							$condition .= $prefixTable . strtolower($part[0]) . "" . $part[1] . "?";
						}
					}
					
					$condition .= " AND ";
					$this->bindArray[] = $part[2];
				}
				$condition = substr($condition, 0, -5);
			}
		}
		
		if($sortBy !== false){
			$condition .= " ORDER by " . $sortBy . " ";
			if($descending == false){
				$condition .= " ASC";
			}
			else {
				$condition .= "DESC";
			}
		}
		
		if($limit !== false && is_numeric($limit)){
			$condition .= " LIMIT " . $limit;
		}
		
		return $condition;
	}
	
	protected function getBindArray(){
		return ($this->bindArray) ? $this->bindArray : array();
	}
	
	/**
	 * Removes html tags and javascript from a string.
	 * @param string $string
	 * @return string
	 */
	public static function stripHarmful($string){
		$patterns = array();
		$patterns[] = "/<script.*>(.|\n)+.*<\/script>/i";
		$patterns[] = "/<form.*>(.|\n)+.*<\/form>/i";
		$patterns[] = "/\sonload=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonclick=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sondblclick=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonmouseout=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonmouseover=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonmouseout=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonunload=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonblur=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonkeyup=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonkeydown=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sondblclick=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonmousemove=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonmouseup=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonmousedown=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonabort=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonsubmit=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonfocus=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonselect=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonreset=(\'|\").*(.*|\n).+(\'|\")/i";
		$patterns[] = "/\sonchange=(\'|\").*(.*|\n).+(\'|\")/i";
		
		foreach($patterns as $pattern){
			$string = preg_replace($pattern, "", $string);
		}
		
		$string = strip_tags($string);
		return $string;

	}
	
	protected function checkTableExists($tableName){
		if($tableName){
			$sql = "";
			switch(Config::getInstance()->databaseDriver){		
				case "mysql":
					$sql = "SELECT table_name from information_schema.tables where table_schema='" . Config::getInstance()->databaseName . "' AND table_name='" . $tableName . "'";
					break;
				case "pgsql":
					$sql = "SELECT table_name from information_schema.tables where table_name='" . $tableName . "'";
					break;
				default: 
					return false;
			}
			
			$data = Database::query($sql);
			return (bool) count($data->fetchAll());
		
		}
		else {
			return false;
		}
	}	
}
