<?php 
class PowerSetup {
	
	private static function getBaseClasses(){
		$pathArr = explode(DIRECTORY_SEPARATOR, __DIR__);
		array_pop($pathArr);
		$pathArr[] = "base";
		$baseURLPath = implode(DIRECTORY_SEPARATOR, $pathArr);
		
		
		$handle = opendir($baseURLPath);
		
		$fileList = array();
		while(false !== ($entry = readdir($handle))){
			if(preg_match("/base/", $entry)){
				$fileList[] = $baseURLPath . DIRECTORY_SEPARATOR . $entry;
			}
		}
		closedir($handle);
		
		$classNameList = array();
		foreach($fileList as $file){
			$classNameList[] = self::getClassNameFromFile($file);
		}
		return $classNameList;
		
	}
	
	private static function getClassNameFromFile($fileName){
		$contents = file_get_contents($fileName);
		$regexp = "/class\s*(.*)\s.*extends/";
		$m = array();
		preg_match($regexp, $contents, $m);
		if(isset($m[1]) && preg_match("/base/i", $m[1])){
			return $m[1];
		}
		else {
			return false;
		}
	}
	
	public static function createTables(){
		foreach(self::getBaseClasses() as $name){
			/*@var $name IPowerModel */
			call_user_func(array($name, 'generateTable'));	
		}
	}
	
	public static function updateTables(){
		foreach(self::getBaseClasses() as $name){
			call_user_func(array($name, 'updateTable'));
		}
	}
}
