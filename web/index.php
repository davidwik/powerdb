<?php 
require ".." . DIRECTORY_SEPARATOR . "includes" . DIRECTORY_SEPARATOR . "bootstrap.php";

$th = new ThemeHelper();
$theme = (strlen(Router::getInstance()->property()->theme)>0) ? Router::getInstance()->property()->theme : Config::getInstance()->defaultTemplate;


if(strcmp(Router::getInstance()->property()->type, "service")){
	ob_start();
	require(ROOT_DIR . DIRECTORY_SEPARATOR .  "web" . DIRECTORY_SEPARATOR . "themes" . DIRECTORY_SEPARATOR .  $theme . DIRECTORY_SEPARATOR . "base.php");
	$content = ob_get_contents();
	ob_end_clean();
	$content = str_replace("<!--HEADTAGS-->", $th->getHeadtags(), $content);
	echo $content;
}
else {
	Router::getInstance()->route(UserState::getInstance()->level);
}