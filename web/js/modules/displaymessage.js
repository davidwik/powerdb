/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
	$(window).resize(function(){
		repositionMessageBox();
	});
	
	repositionMessageBox();
});


function repositionMessageBox(){
	var leftPos = $(window).width() / 2 - $("#messageBox").width()/2;
	$("#messageBox").css("top", "150px");
	$("#messageBox").css("left", leftPos + "px");
}

function showMessageBox(title, text){
	$("#messageBoxBackground").unbind("click");
	$("#messageBoxBackground").fadeTo(300, 0.5, function(){
		$("#messageBoxHeading").html(title);
		$("#messageBoxText").html(text);
		$("#messageBox").slideDown(300, function(){
			$("#messageBoxBackground").bind("click", hideMessageBox);
			setTimeout("hideMessageBox()", 3000);
		});
		
	});
}

function hideMessageBox(){
	$("#messageBox").fadeOut(200, function(){
		$("#messageBoxBackground").fadeOut(200);
	});
}