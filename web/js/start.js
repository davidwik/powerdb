$(document).ready(function(){
	$("#add-more-properties").click(function(){
		var numRows = 0;
		$(".powerdb-row").each(function() {
			numRows++;
		});
		numRows--;
		
		var str = new String(); 
		str += "<div class=\"powerdb-row\">\n";
		str += "<span class=\"powerdb-label\">Property name:</span>\n";
		str += "<input type=\"text\" class=\"property-input\" name=\"property[]\" />";
		str += "<select name=\"type[]\" class=\"selectType\">";
		$.getJSON('types.json', function(data) {
			var items = [];
			$.each(data, function(key, val) {
				str += "\t<option value=\"" + val + "\">" + val + "</option>\n";
			});
			str += "</select><!--<input type=\"checkbox\" name=\"strip[]\" value=\"" + (numRows) + "\" /><label><span class=\"striptag\">Strip harmful?</span></label>-->\n";
			$("#properties").append(str);
			bindOtherAction();
		});
	});
	
	$("#reset").click(function(){
		window.location.assign('start');
	});
	
	$("#powerdb-generated-data-top").click(function(){
		if($("#powerdb-generated-data-block").css('display') == 'none'){
			$("#powerdb-generated-data-block").slideDown(200);
		}
		else {
			$("#powerdb-generated-data-block").slideUp(200, function(){
				$("#powerdb-generated-data-block").select();
			});
		}
		
		
	});
	
	bindOtherAction();

});

function bindOtherAction(){
	$(".selectType").unbind('change');
	
	$(".selectType").bind('change', function(){
		if($(this).val() == 'OTHER'){
			$(this).replaceWith("<input type=\"text\" name=\"type[]\" class=\"selectType\" style=\"width: 80px;\" />");
			bindOtherAction();
		}
		else if($(this).val() == 'SELECT'){
			var sel = $(this);
			var str = new String(); 
				$.getJSON('types.json', function(data) {
				str += "<select name=\"type[]\" class=\"selectType\">";
				var items = [];
				$.each(data, function(key, val) {
					str += "\t<option value=\"" + val + "\">" + val + "</option>\n";
				});
				str += "</select>\n";
				sel.replaceWith(str);
				bindOtherAction();
			});
		}
	});
}


function formCheck(){
	
	var objectName = new String($("#objectName").val());
	
	
	if($("#objectName").val().length < 1){
		showMessageBox("Error", "You need to specify a name for your object");
		return false;
	}
	else {
		if($(".property-input").get(0).value.length < 1){
			showMessageBox("Error", "You need to add at least one property for the object");
			return false;
		}
		else {
			return true;
		}
	}
}