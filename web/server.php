<?php 
ini_set('display_errors', true);

if (preg_match('/\.(?:png|jpg|jpeg|gif|css|js)$/', $_SERVER["REQUEST_URI"])) {
    return false;
} 
else {
	if(isset($_SERVER['REQUEST_URI'])){
		require __DIR__ . '/index.php';
	}
}