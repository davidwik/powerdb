<?php echo("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href='http://fonts.googleapis.com/css?family=Chango' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Droid+Sans+Mono' rel='stylesheet' type='text/css'>
	<!--HEADTAGS-->
</head>
<body>
	<div id="site-wrapper">
		<div id="logo">PowerDB</div>
		<div id="slogan">The lightweight ORM for MySQL and PostGreSQL</div>
	</div>
	<?php Router::getInstance()->route(UserState::getInstance()->level);?>
	<?php SiteModule::get("displaymessage");?>
</body>
</html>
